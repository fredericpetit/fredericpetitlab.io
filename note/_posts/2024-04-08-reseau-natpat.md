---
layout:		post
title:		"NAT / PAT réseau"
date:		2024-04-08 11:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur le NAT / PAT.

> RÉSUMÉ : Le type de NAT le plus commun aujourd'hui est le dynamique PAT utilisé par exemple sur les boxs internet des particuliers.

### I) Définition.

| type | fonction |
| ------ | ------ |
| **masquage**<br />(nat outbound) | permet à une IP privée de **sortir** sur le net. |
| **port forwarding**<br />(nat inbound) | permet à une IP publique de **rentrer** dans un serveur privé. |

| |
| ------ |
| ![natpat]({{ site.baseurl }}/assets/img/note/nat_pat1.png) |

### II) Différents types de NAT.

| type | fonction |
| ------ | ------ |
| **NAT statique** | une IP publique = une machine connectée sur le réseau local. |
| **NAT dynamique** | une IP publique = une machine connectée sur le réseau local,<br />temporairement et ré-adressable. |
| **NAT dynamique PAT** | une IP publique = plusieurs machines connectées sur le réseau local. |

### Sources tierces.

- article de _IT-Connect_ : [https://www.it-connect.fr/le-nat-et-le-pat-pour-les-debutants/](https://www.it-connect.fr/le-nat-et-le-pat-pour-les-debutants/).
- vidéo de _Cookie Connecté_ : [https://www.youtube.com/watch?v=jq3SLuhIyPI](https://www.youtube.com/watch?v=jq3SLuhIyPI).
