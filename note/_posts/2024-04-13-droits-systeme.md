---
layout:		post
title:		"Droits système"
date:		2024-04-12 09:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur les droits système.

> RÉSUMÉ : Les droits de partage s'appliquent uniquement au dossier partagé, tandis que les droits NTFS peuvent être finement règlés et sont à préférer.

### GNU/Linux.

#### I) Droits des utilisateurs.

##### A) commandes.

| commande | format |
| ------ | ------ |
| chmod | SRWE |
| chown | user:group |
| chgroup | group |

##### B) SRWE.

| spécial | proprio | group | others |
| ------ | ------ | ------ | ------ |
| SetUID exec droits proprio, SetGID exec droits groupe, sticky bit swap.| RWE | RWE | RWE |

##### C) RWE numérique.

| droit | valeur |
| ------ | ------ |
| Read | 4 |
| Write | 2 |
| Execute | 1 |

#### II) Gestion des utilisateurs.

##### A) Interactif.

| add/change | del | fichier |
| ------ | ------ | ------ |
| **adduser** [username] | **userdel** [username] | /etc/passwd |
| **addgroup** [groupname / username groupname] | **groupdel** [username groupname] | /etc/group |

##### B) Script.

| add/change | del | fichier |
| ------ | ------ | ------ |
| **useradd** [username] | **userdel** [username] | /etc/passwd |
| **groupadd** [groupname] | **groupdel** [groupname] | /etc/group |
| **usermod** [-G groupname username] | | /etc/group |

### Microsoft Windows.

#### I) NTFS.

1. Ouvrez **l’onglet « Sécurité »**.
1. Dans la boîte de dialogue du dossier « Propriétés », cliquez sur « Modifier ».
1. Cliquez sur le nom de l’objet dont vous souhaitez modifier les autorisations.
1. Sélectionnez « Autoriser » ou « Refuser » pour chacun des paramètres.
1. Pour appliquer les autorisations, cliquez sur « Appliquer ».

#### II) Partage.

1. Cliquez avec le bouton droit sur **le dossier partagé**.
1. Cliquez sur « Propriétés ».
1. Ouvrez **l’onglet « Partage »**.
1. Cliquez sur « Partage avancé ».
1. Cliquez sur « Autorisations ».
1. Sélectionnez un utilisateur ou un groupe dans la liste.
1. Sélectionnez « Autoriser » ou « Refuser » pour chacun des paramètres.

### Sources tierces.

- article de _Linuxaddict_ : [https://www.linuxaddict.fr/index.php/2018/06/04/les-droits-speciaux-sous-gnu-linux-setuid-setgid-sticky-bit-et-umask/](https://www.linuxaddict.fr/index.php/2018/06/04/les-droits-speciaux-sous-gnu-linux-setuid-setgid-sticky-bit-et-umask/).
- article de _Netwrix_ : [https://blog.netwrix.fr/2019/02/28/differences-entre-autorisations-de-partage-et-autorisations-ntfs/](https://blog.netwrix.fr/2019/02/28/differences-entre-autorisations-de-partage-et-autorisations-ntfs/).
- article _IT-Connect_ : [https://www.it-connect.fr/serveur-de-fichiers-les-permissions-ntfs-et-de-partage/](https://www.it-connect.fr/serveur-de-fichiers-les-permissions-ntfs-et-de-partage/).