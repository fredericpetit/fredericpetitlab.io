---
layout:		post
title:		"Réseau mail"
date:		2024-04-08 16:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur le mail.

### I) Définition.

| nom | fonction |
| --- | --- |
| **MUA** (Mail **User Agent**)| Le **client** de messagerie (KMail, Evolution, etc ...). |
| **MTA** (Mail **Transfert Agent**)| L'**agent** qui va **transférer** votre mail vers le serveur chargé de la gestion des emails de votre destinataire,<br />dans la pratique le courrier peut transiter par plusieurs MTA. |
| **MDA** (Mail **Delivery Agent**) | Le **service** de **remise** du courrier dans les boîtes aux lettres des destinataires. |

### II) Protocoles.

| nom | fonction |
| ------ | ------ |
| **POP** | entrant, **stocke** (ports standard 110/995) |
| **IMAP** | entrant, **synchronise** (ports standard 143/993) |
| **SMTP** | sortant, **envoi** (ports standard 25/465) |

### Sources tierces.

- article de _IT-Connect_ : [https://www.it-connect.fr/messagerie-decouverte-des-protocoles-smtp-pop-imap-et-mapi/](https://www.it-connect.fr/messagerie-decouverte-des-protocoles-smtp-pop-imap-et-mapi/)
