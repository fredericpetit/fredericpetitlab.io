---
layout:		post
title:		"Calcul réseau (ip)"
date:		2024-04-08 09:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur le calcul réseau (IP).

### I) Binaire & Hexa.

#### A) Binaire avec puissances de 2.

##### 1) Communs.

| 128<br />(2^7) | 64<br />(2^6) | 32<br />(2^5) | 16<br />(2^4) | 8<br />(2^3) | 4<br />(2^2) | 2<br />(2^1) | 1<br />(2^0) | |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
|1|1|1|1|1|1|1|1| 255 |
|1|1|1|        |        |        |        |        | 224 |
|1|1|        |        |        |        |        |        | 192 |
|1|        |        |        |        |        |        |        | 128 |
|1|        |1|1|        |        |        |        | 176 |
|1|        |1||        |        |        |        | 160 |

#### 2) Masque.

| 128<br />(2^7) | 64<br />(2^6) | 32<br />(2^5) | 16<br />(2^4) | 8<br />(2^3) | 4<br />(2^2) | 2<br />(2^1) | 1<br />(2^0) | |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
||||||||| 0 |
|1|||||||| 128 |
|1|1||||||| 192 |
|1|1|1|||||| 224 |
|1|1|1|1||||| 240 |
|1|1|1|1|1|||| 248 |
|1|1|1|1|1|1||| 252 |
|1|1|1|1|1|1|1|| 254 |
|1|1|1|1|1|1|1|1| 255 |

#### B) Hexadécimal.

| décimal | hexadécimal (4 derniers bits) |
| ------ | ------ |
| 0 | 00 |
| 1 | 01 |
| 2 | 02 |
| 3 | 03 |
| 4 | 04 |
| 5 | 05 |
| 6 | 06 |
| 7 | 07 |
| 8 | 08 |
| 9 | 09 |
| 10 | 0A |
| 11 | 0B |
| 12 | 0C |
| 13 | 0D |
| 14 | 0E |
| 15 | 0F |

Si la valeur porte sur les 4 premiers bits, il faut multiplier le décimal par 16.

### II) Classes d'IP.

#### A) Définition.

> La suite des protocoles Internet est l'ensemble des protocoles utilisés pour le transfert des données sur Internet. Elle est aussi appelée suite TCP/IP, DoD Standard (DoD pour Department of Defense) ou bien DoD Model ou encore DoD TCP/IP ou US DoD Model. Elle est souvent appelée TCP/IP, d'après le nom de ses deux premiers protocoles : TCP (de l'anglais Transmission Control Protocol) et IP (de l'anglais Internet Protocol). Ils ont été inventés par Vinton G. Cerf et Bob Kahn, travaillant alors pour la DARPA, avec des influences des travaux de Louis Pouzin. Le document de référence est RFC 1122.

source : [https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet](https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet)

> La distinction entre les adresses de classe A, B ou C a été ainsi rendue obsolète, de sorte que la totalité de l'espace d'adressage unicast puisse être gérée comme une collection unique de sous-réseaux indépendamment de la notion de classe. Le masque de sous-réseau ne peut plus être déduit de l'adresse IP elle-même, les protocoles de routage compatibles avec CIDR, dits classless, doivent donc accompagner les adresses du masque correspondant. C'est le cas de Border Gateway Protocol dans sa version 4, utilisé sur Internet (RFC 1654 A Border Gateway Protocol 4, 1994), OSPF, EIGRP ou RIPv2. Les registres Internet régionaux (RIR) adaptent leur politique d'attribution des adresses en conséquence de ce changement. L'utilisation de masque de longueur variable (Variable-Length Subnet Mask, VLSM) permet le découpage de l'espace d'adressage en blocs de taille variable, permettant une utilisation plus efficace de l'espace d'adressage.

source : [https://fr.wikipedia.org/wiki/Adresse_IP#Agr%C3%A9gation_des_adresses](https://fr.wikipedia.org/wiki/Adresse_IP#Agr%C3%A9gation_des_adresses)

#### B) Listing.

| classe | plage | bit(s) de départ dans l'IP | masque par défaut<br />(traduire en binaire)<br />+ CIDR<br />(par rapport au nbr de bits à 1 dans masque à partir de la gauche et en continu) | max. réseaux<br />NET-ID<br />(par rapport au masque) | max. machines<br />HOST-ID<br />(par rapport au masque) | privé |
| ------ | ------ | ------ |  ------ | ------ | ------ | ------ | 
| A<br />(128) | **0**.0.0.0 à<br />**127**.255.255.255 | 0 | **255**.0.0.0<br />1 octet à 255<br />= 8 bits à 1<br />= **/8** | bit de départ à 1,<br/>suivi de 7 bits à 0<br />= 2^7<br />= 128 | 24 bits à 0<br />= 2^24<br />= 16 777 214 (-2) | 10.0.0.0 à 10.255.255.255,<br />127.0.0.0 à 127.255.255.255 |
| B<br />(64) | **128**.0.0.0 à<br />**191**.255.255.255 | 10 | **255.255**.0.0<br />2 octets à 255<br />= 16 bits à 1<br />= **/16** | bit de départ à 10,<br/>suivi de 14 bits à 0<br /> = 2^14<br />= 16 384 | 16 bits à 0<br />= 2^16<br />= 65 534 (-2) | 172.16.0.0 à 172.31.255.255 |
| C<br />(32) | **192**.0.0.0 à<br />**223**.255.255.255 | 110 | **255.255.255**.0<br />3 octets à 255<br />= 24 bits à 1<br />= **/24** | bit de départ à 110,<br/>suivi de 21 bits à 0<br /> = 2^21<br />= 2 millions | 8 bits à 0<br />= 2^8<br />= 254 (-2) | 192.168.0.0 à 192.168.255.255 |
| D<br />(16) | **224**.0.0.0 à<br />**239**.255.255.255 | 1110 | **255.255.255.255**<br />4 octets à 255<br />= 32 bits à 1<br />= **/32** | x | x | x |
| E<br />(16) | **240**.0.0.0 à<br />**255**.255.255.255 | 1111 | x | x | x | x |
| 256 | | | | | | |

### III) CIDR.

| cidr1 | cidr2 |
| ------ | ------ |
| ![cidr1]({{ site.baseurl }}/assets/img/note/cidr1.png) | ![cidr2]({{ site.baseurl }}/assets/img/note/cidr2.png) |

### IV) Calcul masque de sous-réseau.

#### A) Méthode full binaire (@-IP = 192.168.0.10 / 24).

| type | adresse | binaire |
| ------ | ------ | ------ |
| **adresse IPv4** | 192.168.0.10 | 11000000 10101000 00000000 00001010 |
| **masque de sous-réseau** | 255.255.255.0 | 11111111 11111111 11111111 00000000 |
| **adresse réseau** | 192.168.0.0 | 11000000 10101000 00000000 00000000  |
| **hôtes disponibles** | 8 bits à zéro dans le masque = 2^8 = 256-2 = 254 | 00000000 |

#### B) Méthode perso (@-IP = 100.191.6.33 / 13).

##### @-RESEAU.

1. traduction par blocs de 8 du **@-MASQUE** et de **@-IP**
2. note des 2^ des bits à 0 dans l’octet de travail du **@-MASQUE**
3. octets de travail **@-IP** / **@-MASQUE** : 1011-1111 / 1111-1000 = 1011-1000 = 184
4. **@-RESEAU** = 100.184.0.0

##### @-BROADCAST.

1. 2^nombre de bits à 0 dans l’octet de travail du **@-MASQUE** = 2³ = 8
2. valeur octet de travail de **@-RESEAU** + une plage - 1 = 184 + 8 - 1
3. **@-BROADCAST** = 100.191.255.255

### VI) Notes.

**Général**
- octet = byte
- Bit de poids fort le premier, Bit de poids faible le dernier.

**Valeurs**
- 1 octet = 8 bits
- 4 octets (**@-IP** IPV4) = 32 bits
- 6 octets (**@-MAC**) = 48 bits
- 16 octets (**@-IP** IPV6) = 128 bits

**Définitions**
- le masque détermine partie réseau (fixe) **255...** et partie hôte (variable) **..0**
- l'adresse réseau finit part 0, jamais attribuable
- l'adresse broadcast finit (par défaut) par 255, jamais attribuable

### Sources tierces.

- calculateur binaire hexadécimal : [https://sebastienguillon.com/test/javascript/convertisseur.html](https://sebastienguillon.com/test/javascript/convertisseur.html).
- article calcul masque : [https://www.it-connect.fr/adresses-ipv4-et-le-calcul-des-masques-de-sous-reseaux/](https://www.it-connect.fr/adresses-ipv4-et-le-calcul-des-masques-de-sous-reseaux/).
- article calcul masque : [https://web.maths.unsw.edu.au/~lafaye/CCM/internet/ip.htm](https://web.maths.unsw.edu.au/~lafaye/CCM/internet/ip.htm).
- article découpage : [https://www.inetdoc.net/articles/adressage.ipv4/adressage.ipv4.exercises.html](https://www.inetdoc.net/articles/adressage.ipv4/adressage.ipv4.exercises.html).
- vidéo adresse IP et masques de sous-réseaux : [https://www.youtube.com/watch?v=RnpSaDSSjR4](https://www.youtube.com/watch?v=RnpSaDSSjR4).
- calculateur correcteur réseau masque : [https://cric.grenoble.cnrs.fr/Administrateurs/Outils/CalculMasque/](https://cric.grenoble.cnrs.fr/Administrateurs/Outils/CalculMasque/).
- calculateur correcteur subneting masque : [https://www.calculator.net/ip-subnet-calculator.html](https://www.calculator.net/ip-subnet-calculator.html).
- vidéo de _Cookie Connecté_ : [https://www.youtube.com/watch?v=26jazyc7VNk](https://www.youtube.com/watch?v=26jazyc7VNk).