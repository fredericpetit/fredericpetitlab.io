---
layout:		post
title:		"Découpage réseau (ip)"
date:		2024-04-08 10:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur le découpage réseau.

> RÉSUMÉ : Pour faire le découpage réseau on se sert du binaire et des puissances de 2.

### IP.IP.IP.IP / @-MASQUE pour déduire @-RESEAU et @-BROADCAST.

- Découpage de **@-MASQUE** en 4 octets ? + [ ? + ? ] + ? + ? .
- **NID** et **HID** en puissances 2^ "pleines".
- **@-RESEAU** et **@-BROADCAST** (plage) en puissances 2^ dans l'octet de travail.
- Comparaison binaire de l'octet de travail dans le **@-MASQUE** et **@-IP** pour déduire **@-RESEAU**. On garde les bits à 1 qui sont similaires.
- Ajout de **@-BROADCAST** (plage) - 1 adresse à **@-RESEAU** pour déduire **@-BROADCAST** (fin).

### Découpages.

#### A) Découpage net-ID.

- Principe : **on rajoute des bits à 1 par rapport à @-MASQUE donné**.
- Formule : **nombre de sous-réseaux voulu = 2^ ?**, nouveau masque = **masque actuel + ?**.

#### B) Découpage host-ID.

- Principe : **on rajoute des bits à 0 par rapport à /32**.
- Formule : **nombre de machines voulu = 2^ ?** (-2 adresses pour **@-RESEAU** et **@-BROADCAST**), nouveau masque = **32 - ?**.