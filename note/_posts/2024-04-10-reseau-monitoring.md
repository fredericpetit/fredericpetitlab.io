---
layout:		post
title:		"Monitoring réseau"
date:		2024-04-10 09:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur le monitoring.

> RÉSUMÉ : Le protocole SNMP (Simple Network Management Protocol) est un protocole standard de gestion réseau utilisé pour surveiller et gérer les équipements réseau tels que les routeurs, commutateurs, serveurs, imprimantes, et autres dispositifs compatibles sur un réseau IP.

### I) Protocole SNMP.

#### A) Définition.

SNMP (Simple Network Management Protocol) pour monitorer des équipements et alerter sur la moindre défaillance, afin de :

- Réduire les périodes d’indisponibilité.
- Fournir à tout moment l’état du réseau et de ses composants.
- Inventorier les équipements.
- Quantifier et optimise les trafics.
- Faciliter la planification des interventions sur le réseau.
- Permettre une automatisation des procédures de correction des problèmes.

#### B) Composition.

- serveur avec **gestionnaire** et **MIB (Managed Information Base)** (base de données).
- client(s) avec **agent** disposant d'un **OID (Objectfs IDentifier)**.

#### C) Fonctionnalités.

| type | vérification | port |
| ------ | ------ | ------ |
| polling | active, régulière par script | UDP 161 |
| trap | passive, sur alerte | UDP 162 |

### II) Autres produits.

- NetFlow / sFlow : Mieux pour l'analyse du trafic réseau en temps réel.
- WMI / IPMI : Plus orientés vers la gestion des systèmes Windows ou des serveurs.
- REST API : Approche moderne pour interagir avec des équipements via des interfaces web.
- Nagios / Prometheus / Zabbix : Outils de surveillance avec des capacités plus larges, intégrant souvent SNMP mais aussi d'autres méthodes plus récentes et flexibles.