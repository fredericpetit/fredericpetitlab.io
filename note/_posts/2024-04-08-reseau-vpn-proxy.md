---
layout:		post
title:		"Réseau VPN + Proxy"
date:		2024-04-08 15:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur les VPN & Proxy.

> RÉSUMÉ : 3 types de VPN et 2 types de Proxy.

### I) Définition.

#### A) VPN.

| type | fonction |
| ------ | ------ |
| **client-to-site** | Accés depuis l'extérieur à une **ressource interne**. |
| **site-to-site** | **Interconnexion** de deux réseaux. |
| **grand public** | **Contourner** les géo-restrictions via un fournisseur VPN. |

#### B) Proxy.

##### 1) Généralité.

**Forward** & **Reverse**.

| |
| ------ |
| ![proxy]({{ site.baseurl }}/assets/img/note/proxy.png) |

##### 2) ACL Squid.

Dans _/etc/squid/squid.conf_, pour autoriser un réseau et interdire x.com :

- `acl ACL0 src 192.168.1.0/24`
- `http_access allow ACL0`
- `http_access deny all`
- `acl ACL1 dstdomain .x.com`
- `https_access deny ACL1`

Tester avec `squid -k parse|check`.

### II) Protocoles VPN.

| nom | fonction |
| ------ | ------ |
| **OpenVPN** | Le plus courant et généralement très largement conseillé. |
| **IKEv2, L2P/IPSec et PPTP** | Très présents. |
| **SSTP** | Protocole de Microsoft accessible sur tous les ordinateurs Windows. |
| **PPTP** | Protocole historique, mais obsolète. |

### Sources tierces.

- article de _IT-Connect_ : [https://www.it-connect.fr/les-tunnels-vpn-pour-les-debutants/](https://www.it-connect.fr/les-tunnels-vpn-pour-les-debutants/).
- vidéo _Squid_ par _EASI_ : [https://www.youtube.com/watch?v=M4-nDuIB2FI](https://www.youtube.com/watch?v=M4-nDuIB2FI).
