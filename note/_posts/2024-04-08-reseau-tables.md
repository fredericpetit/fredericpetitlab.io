---
layout:		post
title:		"Tables réseau"
date:		2024-04-08 12:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur les tables ARP / CAM / Routage.

> RÉSUMÉ : Chaque équipement posséde sa table ARP, les SWITCHS ont une table CAM, et les ROUTEURS ont une table de ROUTAGE.

### I) Définition.

| table | éléments | équipement |
| ------ | ------ | ------ |
| ARP | **@-IP** + **@-MAC** + type (statique ou dynamique) | tous |
| CAM | port (du switch) + **@-MAC** | switch |
| Routage | **@-RESEAU** + **@-MASQUE** + **@-PASSERELLE** + **@-INTERFACE** | routeur |

| | |
| ------ | ------ |
| ![table]({{ site.baseurl }}/assets/img/note/table_arp.png) | |
| ![table]({{ site.baseurl }}/assets/img/note/table_cam.png) | ![table]({{ site.baseurl }}/assets/img/note/table_routage.png) |

### II) Exemple.

| |
| ------ |
| ![routage]({{ site.baseurl }}/assets/img/note/routage.png) | |

#### A)

| RÉSEAU | MASQUE | PASSERELLE | INTERFACE |
| ------ | ------ | ------ | ------ |
| 192.168.192.0 | 24 | - | 192.168.192.1 |
| 0.0.0.0 | 0 | 192.168.192.251 | 192.168.192.1 |

#### R1)

| RÉSEAU | MASQUE | PASSERELLE | INTERFACE |
| ------ | ------ | ------ | ------ |
| 10.0.0.0 | 8 | 172.16.10.3 | 172.16.10.1 |
| 172.16.0.0 | 16 | - | 172.16.10.1 |
| 192.168.192.0 | 24 | - | 192.168.192.251 |

#### R2)

| RÉSEAU | MASQUE | PASSERELLE | INTERFACE |
| ------ | ------ | ------ | ------ |
| 10.0.0.0 | 8 | - | 10.1.1.1 |
| 172.16.0.0 | 16 | - | 172.16.10.3 |
| 192.168.192.0 | 24 | 172.16.10.1 | 172.16.10.3 |

#### B)

| RÉSEAU | MASQUE | PASSERELLE | INTERFACE |
| ------ | ------ | ------ | ------ |
| 10.0.0.0 | 8 | - | 10.1.1.23 |
| 0.0.0.0 | 0 | 10.1.1.1 | 10.1.1.23 |

### III) Notes.

- L'**@-IP** est chargée avec l'OS tandis que l'**@-MAC** est chargée dès le BIOS.
- Dans une ARP Request l'**@-MAC** source est la passerelle.
- Dans une table de routage, pour joindre **@-RESEAU** de masque **@-MASQUE** je passe par **@-PASSERELLE** via **@-INTERFACE**, et l'adresse **@-PASSERELLE** doit toujours être dans le même réseau que son **@-INTERFACE**.

### Sources tierces.

- vidéo de _Prométhée Spathis_ : [https://www.youtube.com/watch?v=ivAuTsFXByo](https://www.youtube.com/watch?v=ivAuTsFXByo).