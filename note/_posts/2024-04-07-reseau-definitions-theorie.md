---
layout:		post
title:		"Définitions réseau (théorie)"
date:		2024-04-07 09:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur le réseau (théorie : modèles) OSI et TCP/IP).

> RÉSUMÉ : Les **ROUTEURS de couche 3 gérent l'adressage logique** tandis que les **SWITCHS de couche 2 gérent l'adressage physique**. Les protocoles de haut-niveau y sont transportés par les protocoles de bas-niveau, dans des trames contenant des paquets contenant des segments.

<table style="font-size: 12px;">
	<thead>
		<tr>
			<th nowrap>Protocol Data Unit</th>
			<th nowrap>#</th>
			<th>fonction</th>
			<th>matériel</th>
			<th>application</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td rowspan=3><br /><br /><br />Donnée</td>
			<td nowrap>7 - <b>Application</b></td>
			<td>Point d'accès aux services réseau.</td>
			<td></td>
			<td rowspan=3><br /><br /><br /><b>Protocoles haut-niveau</b> avec les navigateurs web, clients Mail/FTP, etc (exemple HTTP en couche 7 et HTML en couche 6).<br /><br />en <b>TCP</b> : HTTP (80), HTTPS (443), SSH (22), FTP (20 data / 21 command), POP (110/995), IMAP (ports 143/993), SMTP (ports 25/465),<br />en <b>UDP</b> : DNS (53), DHCP (67), SNMP (161 / 162).</td>
		</tr>
		<tr>
			<td nowrap><nobr>6 - <b>Présentation</b></nobr></td>
			<td>Gère le chiffrement et le déchiffrement des données,<br />convertit les données machine en données exploitables par n'importe quelle autre machine.</td>
			<td></td>
		</tr>
		<tr>
			<td>5 - <b>Session</b></td>
			<td>Communication Interhost,<br />gère les sessions entre les différentes applications.</td>
			<td></td>
		</tr>
		<tr>
			<td>Segment TCP/UDP<br /></td>
			<td>4 - <b>Transport</b></td>
			<td>Connexion de bout en bout, connectabilité et contrôle de flux ;<br />notion de port (TCP & UDP).</td>
			<td></td>
			<td><b>Protocoles bas-niveau</b> de la suite TCP/IP : <b>IP</b> Internet Protocol, <b>TCP</b> Transmission Control Protocol (segment synchronisé) & <b>UDP</b> User Datagram Protocol (datagramme non synchronisé).</td>
		</tr>
		<tr>
			<td>Paquet IP</td>
			<td>3 - <b>Réseau</b></td>
			<td>Détermine le parcours des données,<br />Adressage <b>logique</b> (<b>@-IP</b>).</td>
			<td><b>ROUTEUR</b> :<br />- communique avec l'extérieur<br />- traite les <b>@-IP</b></td>
			<td>suite TCP/IP : <b>ICMP</b> (ping, traceroute), <b>ARP</b> (mappage @-IP/@-MAC).</td>
		</tr>
		<tr>
			<td>Trame Ethernet</td>
			<td>2 - <b>Liaison</b></td>
			<td>Adressage <b>physique</b> (<b>@-MAC</b>).</td>
			<td nowrap><b>SWITCH</b> :<br />- commutateur<br />- full-duplex<br />- traite les <b>@-MAC</b></td>
			<td>Ethernet, <b>ARP</b> (mappage @-IP/@-MAC).</td>
		</tr>
		<tr>
			<td>Bit / Symbole</td>
			<td>1 - <b>Physique</b></td>
			<td>Transmission des signaux sous forme numérique ou analogique.</td>
			<td nowrap><b>HUB</b> :<br />- concentrateur<br />- half-duplex<br />- traite les bits</td>
			<td>Câblage & WiFi.</td>
		</tr>
	</tbody>
</table>

## I) Définition.

**Un protocole est une règle de dialogue entre deux ou plusieurs machines définies par un port et un language.**

> Le modèle OSI (de l'anglais Open Systems Interconnection) est une norme de communication, en réseau, de tous les systèmes informatiques. C'est un modèle de communications entre ordinateurs proposé par l'ISO (Organisation internationale de normalisation) qui décrit les fonctionnalités nécessaires à la communication et l'organisation de ces fonctions.

source : [https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI).

> Le modèle OSI est générique, indépendant du protocole, mais la plupart des protocoles et des systèmes y adhèrent, tandis que le modèle TCP/IP est basé sur des protocoles standard que l'Internet a développés. Le modèle TCP/IP et le modèle OSI sont tous deux des modèles conceptuels utilisés pour la description de toutes les communications réseau, tandis que TCP/IP lui-même est également un protocole important utilisé dans toutes les opérations Internet. Généralement, lorsqu'on parle de la couche 2, de la couche 3 ou de la couche 7 dans laquelle un périphérique réseau fonctionne, nous faisons référence au modèle OSI. Le modèle TCP/IP est utilisé à la fois pour modéliser l'architecture Internet actuelle et pour fournir un ensemble de règles qui sont suivies par toutes les formes de transmission sur le réseau.

source : [https://community.fs.com/fr/blog/tcpip-vs-osi-whats-the-difference-between-the-two-models.html](https://community.fs.com/fr/blog/tcpip-vs-osi-whats-the-difference-between-the-two-models.html).

## II) Illustrations.

| | |
| ------ | ------ |
| ![osi2]({{ site.baseurl }}/assets/img/note/osi2.png) | ![udp_tcp3]({{ site.baseurl }}/assets/img/note/udp_tcp3.png) |
| ![osi1]({{ site.baseurl }}/assets/img/note/osi1.png) | |

## III) Protocoles TCP (handshake et synchronisé) & UDP (non synchronisé).

| | |
| ------ | ------ |
| ![udp_tcp2]({{ site.baseurl }}/assets/img/note/udp_tcp2.png) | ![udp_tcp1]({{ site.baseurl }}/assets/img/note/udp_tcp1.png) |

## IV) Encapsulation.

| |
| ------ |
| ![encapsulation]({{ site.baseurl }}/assets/img/note/trame_osi.png) |

### Trajets.

1. **ENVOI** (descend) : segment 4 (tcp-udp) -> paquet 3 (routeur) -> trame 2 (switch).
2. **RÉCEPTION** (remonte) : trame 2 (switch) -> paquet 3 (routeur) -> segment 4 (tcp-udp).

| |
| ------ |
| ![chemin]({{ site.baseurl }}/assets/img/note/chemin.png) |

| TCP | UDP |
| ------ | ------ |
| ![encapsulation]({{ site.baseurl }}/assets/img/note/encapsulation_tcp1.png) | ![encapsulation]({{ site.baseurl }}/assets/img/note/encapsulation_udp1.png) |

## Sources tierces.

- article de _Malekal_ : [https://www.malekal.com/protocole-tcp-udp-icmp-fonctionnement-et-differences](https://www.malekal.com/protocole-tcp-udp-icmp-fonctionnement-et-differences).
- vidéo de _Cookie Connecté_ : [https://www.youtube.com/watch?v=26jazyc7VNk](https://www.youtube.com/watch?v=26jazyc7VNk).