---
layout:		post
title:		"Commandes réseau"
date:		2024-04-08 17:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur les commandes Réseau.

> RÉSUMÉ : on prend connaissance de la configuration, on vérifie ce qui est actif, on pingue @-IP et @-DOMAINE puis on vérifie les @-DNS, les chemins empruntés et enfin les tables.

### I) Procédure de panne.

| # | fonction | commande GNU/Linux | commande Windows |
| ------ | ------ | ------ | ------ |
| 1 | afficher la configuration IP | **ip a** + **resolvectl** | **ipconfig** (/all)|
| 2 | afficher les ports actifs tcp/udp | **ss** | **netstat** |
| 3 | vérifier la connectivité | **ping** (TTL par défaut 64) | **ping** (TTL par défaut 128) |
| 4 | vérifier la connectivité | **nslookup** | **nslookup** |
| 5 | vérifier la connectivité | **tracert** | **traceroute** |
| 6 | vérifier la table @-IP = @-MAC | **ip neighbour** | **arp -a** |

#### Hypothèses.

- **échec** @-DOMAINE + @-IP = **physique** (pare feu, câble ...)
- **succès** @-DOMAINE + @-IP = **serveur** (httpd, dns ...)

### II) Commandes CISCO.

#### A) Principales.

| commande | fonction | machine |
| ------ | ------ | ------ |
| **en** | active | Routeur / Switch |
| **conf t** | entre en monde configuration | Routeur / Switch |
| **show running-config** | montre la configuration | Routeur / Switch |
| **copy running-config startup-config** | sauvegarde la configuration | Routeur / Switch |
| **sh ip route** / **ip route** | pour voir la table de routage / ajouter une route | Routeur |
| **interface** [fa0/24] / [range fa0/7 - 12] | ajoute / configure une interface réseau | Routeur / Switch |
| **switchport mode** [access] (unique) / [trunk] (plusieurs) | configure le mode du port | Routeur / Switch |
| **switchport access** [vlan 10] | configure l'appartenance du port | Routeur / Switch |

| |
| ------ |
| ![vlan1]({{ site.baseurl }}/assets/img/note/vlan1.png) |

#### B) Exemple définir et nommer un vlan - exo.
- **vlan 10**
- **name Production**

#### C) Exemple rattacher un port à un vlan (switch) - exo.
- **interface fa0/1**
- **switchport mode access**
- **switchport access vlan 10**

#### D) Configurer un port en mode trunk (switch) - exo.
- **interface fa0/24**
- **switchport mode trunk**

#### E) Configurer le mode trunk 802.1Q (routeur) pour du routage inter-vlan.

##### 1) Activation de l’interface physique.
- **interface fa0/0**
- **no shutdown**

##### 2) Création et étiquetage de la sub-interface pour le VLAN 10.
- **interface fa0/0.10**
- **encapsulation dot1q 10**
- **ip address 172.26.10.254 255.255.255.0**
- **exit**

##### 3) Création et étiquetage de la sub-interface pour le VLAN 20.
- **interface fa0/0.20**
- **encapsulation dot1q 20**
- **ip address 172.26.20.2054 255.255.255.0**
- **exit**

#### F) Activer le spanning-tree 802.1D pour éviter la tempête de broadcast (saturation réseau et risque de DoS).
- **spanning-tree mode rapid-pvst**

### III) Commandes Iptables.

- **table** : FILTER, NAT ou MANGLE.
- **chaîne** :  PREROUTING, INPUT, FORWARD, OUTPUT ou POSTROUTING.
- **motif** : avec les STATE (NEW, ESTABLISHED, INVALID ou RELATED) .
- **cible** : ACCEPT, DROP, REJECT, LOG ...

### Sources tierces.

- iptables generator : [https://iptablesgenerator.totalbits.com/](https://iptablesgenerator.totalbits.com/).
- article _Ciscomadesimple_ : [https://www.ciscomadesimple.be/2014/03/11/configuration-avancee-de-trunks-dot1q/](https://www.ciscomadesimple.be/2014/03/11/configuration-avancee-de-trunks-dot1q/).
- vidéos _Packet Tracer_ par _Victor Licot_ : [https://www.youtube.com/watch?v=jModo_zdx1c&list=PLbLWLa0S9boRxkGg9SaRylFsLjYE7BfYj](https://www.youtube.com/watch?v=jModo_zdx1c&list=PLbLWLa0S9boRxkGg9SaRylFsLjYE7BfYj).