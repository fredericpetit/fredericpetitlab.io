---
layout:		post
title:		"Boot système & réseau"
date:		2024-04-05 09:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur les boot.

> RÉSUMÉ : Le processus de démarrage d'un système avec un BIOS classique suit plusieurs étapes bien définies.

### I) Boot système.

1. Power-On & POG : émis par l'alimentation pour indiquer que l'alimentation est stable et prête à être utilisée, le processeur reçoit ce signal et commence l'exécution du programme contenu dans le BIOS.
1. Exécution du BIOS : le processeur démarre à une adresse fixe, souvent 0xFFFF0, qui est un pointeur vers le BIOS, et commence l'exécution du code de démarrage.
1. POST - Power On Self Test :  ensemble de tests matériels qui sont effectués pour vérifier que tous les composants matériels de base fonctionnent correctement avant de démarrer le système d'exploitation.
1. Recherche des périphériques de démarrage.
1. Chargement du Bootloader.
1. Transfert au Bootloader et démarrage de l'OS.

#### 0x7c00.

L'adresse mémoire 0x7C00 est une adresse bien connue dans le contexte des systèmes d'exploitation, en particulier pour le bootloader. Voici pourquoi elle est importante :

- Convention historique : Lors du démarrage d'un ordinateur x86 en mode réel (Real Mode), le BIOS (Basic Input/Output System) charge le premier secteur du périphérique de démarrage (habituellement le disque) en mémoire. Ce premier secteur est appelé Master Boot Record (MBR) et a une taille de 512 octets.
- Chargement en mémoire : Par convention, le BIOS charge ce secteur de 512 octets à l'adresse mémoire 0x7C00. Cela permet au code du bootloader d’être placé dans une région de mémoire connue, où il pourra ensuite être exécuté par le processeur.
- Espace en mémoire : L'adresse 0x7C00 a été choisie car elle se trouve en dehors des premières 1 Ko de mémoire (0x0000 à 0x03FF), qui sont réservées pour les interruptions et autres usages du BIOS. Elle laisse également de la place au-dessus de cette adresse pour charger d'autres composants du système.
- Compatibilité : Beaucoup de systèmes d'exploitation et de bootloaders suivent cette convention pour rester compatibles avec le BIOS des systèmes x86. Cela permet au code du bootloader de fonctionner de manière universelle sur les systèmes utilisant cette architecture.

Ainsi, l'adresse 0x7C00 est devenue une norme pour le chargement des bootloaders dans les systèmes x86.

### II) Boot d'un paquet réseau.

1. ET logique entre IP et son masque.
1. Check : table de routage (si pas dans le réseau).
1. Check : table ARP.
1. Envoi d'une ARP Request si inconnu (enregistrée dans table CAM par le switch) + Réception d'une ARP Reply.
1. Informations par la Box qui fait rôle de routeur.
1. Arrive sur le serveur.

### III) Boot matériel : exemple d'un routeur.

1. **Vérification**.
1. **Lancement** - **Chargement**.
1. **Exécution**.

### Sources tierces.

- page _Wikipédia_ : [https://fr.wikipedia.org/wiki/D%C3%A9marrage_d%27un_ordinateur](https://fr.wikipedia.org/wiki/D%C3%A9marrage_d%27un_ordinateur).
- article de _DELL_ : [https://www.dell.com/support/kbdoc/fr-ed/000128270/proc%c3%a9dures-post-et-de-d%c3%a9marrage](https://www.dell.com/support/kbdoc/fr-ed/000128270/proc%c3%a9dures-post-et-de-d%c3%a9marrage).