---
layout:		post
title:		"VmWare Workstation & VirtualBox"
date:		2024-04-04 09:00:00 +0200
categories:	note
author:		"Frédéric"
---

## Note généraliste sur les logiciels VmWare Workstation et VirtualBox.

### Modes.

> RÉSUMÉ : Le BRIDGE simule, le NAT masque, l'HOST-ONLY réserve, le LAN SEGMENT isole.

| vmware | virtualbox |
| ------ | ------ |
| ![récap]({{ site.baseurl }}/assets/img/note/VMware-Workstation-Pro-Recapitulatif-connexion-reseau.png) | ![récap]({{ site.baseurl }}/assets/img/note/virtualbox-synthese-modes-reseau.png) |

| mode | illustration |
| ------ | ------ |
| NAT, avec traduction - carte réseau virtuelle | ![nat]({{ site.baseurl }}/assets/img/note/VMware-Workstation-Pro-Schema-NAT.png) |
| BRIDGE PONT, à poil - carte réseau physique | ![bridge]({{ site.baseurl }}/assets/img/note/VMware-Workstation-Pro-Schema-Bridged.png) |
| HOST ONLY - carte réseau virtuelle | ![host]({{ site.baseurl }}/assets/img/note/VMware-Workstation-Pro-Schema-Host-Only.png) |
| LAN SEGMENT - carte réseau virtuelle | ![host]({{ site.baseurl }}/assets/img/note/VMware-Workstation-Pro-Schema-LAN-segment.png) |

### Sources tierces.
- article de _IT-Connect_ : [https://www.it-connect.fr/comprendre-les-differents-types-de-reseaux-virtualbox/](https://www.it-connect.fr/comprendre-les-differents-types-de-reseaux-virtualbox/).
- article de _IT-Connect_ : [https://www.it-connect.fr/comprendre-les-differents-types-de-reseaux-de-vmware-workstation-pro/](https://www.it-connect.fr/comprendre-les-differents-types-de-reseaux-de-vmware-workstation-pro/).