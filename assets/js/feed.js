// basics.
const RSS_URL1 = `https://fredericpetit.fr/assets/rss/lemagit.xml`;
const RSS_URL2 = `https://fredericpetit.fr/assets/rss/developpez.xml`;
const feeder1 = document.getElementById("feed-lemagit");
const feeder2 = document.getElementById("feed-developpez");

// temp loaders.
const htmlGeneric = `
<div class="live-odd">
	<div class="wrapper-content"><div class="spinner-border" role="status"><span class="sr-only">chargement ...</span></div></div>
</div>
<div class="live-odd">
	<div class="wrapper-content"><div class="spinner-border" role="status"><span class="sr-only">chargement ...</span></div></div>
</div>
<div class="live-odd">
	<div class="wrapper-content"><div class="spinner-border" role="status"><span class="sr-only">chargement ...</span></div></div>
</div>
`;
feeder1.insertAdjacentHTML("beforeend", htmlGeneric);
feeder2.insertAdjacentHTML("beforeend", htmlGeneric);

setTimeout(function() { fetchRSS(RSS_URL1, feeder1); }, 2000);
setTimeout(function() { fetchRSS(RSS_URL2, feeder2); }, 2000);

// limit show text.
function reduceSentence(sentence) {
	let words = sentence.split(' ');
	let limit = 20;
	let firstWords = words.slice(0, 20);
	if (words.length <= limit) {
		return sentence;
	} else {
		let result = words.slice(0, limit).join(" ");
		return result + " ...";
	}
}

// go RSS !
function fetchRSS(url, feeder) {
	fetch(url)
	.then(response => response.text())
	.then(str => new window.DOMParser().parseFromString(str, "text/xml"))
	.then(data => {

		let items = data.querySelectorAll("item");
		let html = ``;
		let limit = 0;

		// loop.
		items.forEach(el => {
			let elTitle = el.querySelector("title").innerHTML.replace("<![CDATA[", "").replace("]]>", "");
			// test : antispam.
			if (!elTitle.includes("OpenAI") && !elTitle.includes("ChatGPT") && !elTitle.includes("Altman") && !elTitle.includes("LLM") && !elTitle.includes("artificielle")) {
				// test : human readable.
				if (limit < 12) {
					limit++;
					let elLink = el.querySelector("link").innerHTML;
					let elTitleFormat = reduceSentence(elTitle);
					let elDate = el.querySelector("pubDate").innerHTML;
					let date = new Date(elDate);
					let dayformat = new Intl.DateTimeFormat('fr', { weekday: 'long' });
					let day = dayformat.format(date);
					let months = Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
					let elDateFormat = day + " " + date.getDate() + " " + months[date.getMonth()];
					html += `
					<a href="${elLink}" target="_blank" class="article" alt="article" title="${elTitleFormat}">
						<div class="wrapper-content">
							<p class="article">${elTitleFormat}</p>
							<p class="date"><i class="fa-regular fa-calendar-days"></i>${elDateFormat}</p>
						</div>
					</a>
					`;
				}
			}
		});

		feeder.querySelectorAll(".live-odd").forEach(rem => rem.remove());
		feeder.insertAdjacentHTML("beforeend", html);
	});
}
