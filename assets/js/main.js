//############################
//# by : fredericpetit.fr
//############################
//# script : main.js
//# usage : main script
//############################

// #################### https location ####################
// gitlab pages doesn't support redirect www to no-www.
if (window.location.host.startsWith("www.")) {
	window.location.replace(window.location.href.replace("www.", ""))
}

// #################### dark mode ####################
// src : https://css-tricks.com/a-complete-guide-to-dark-mode-on-the-web/#combining
function changeClass(element, oldClass, newClass) {
	var state = document.getElementById(element);
	state.classList.remove(oldClass);
	state.classList.add(newClass);
}
const btn = document.querySelector("#btn-toggle");
const prefersDarkScheme = window.matchMedia("(prefers-color-scheme: dark)");
// current theme
const currentTheme = localStorage.getItem("theme");
if (currentTheme == "dark") {
	console.log("fredericpetit.fr :: theme initiated to dark.");
	document.body.classList.toggle("dark-theme");
	changeClass("icon-toggle", "fa-spinner", "fa-moon");
} else if (currentTheme == "light") {
	console.log("fredericpetit.fr :: theme initiated to light.");
	document.body.classList.toggle("light-theme");
	changeClass("icon-toggle", "fa-spinner", "fa-sun");
} else {
	console.log("fredericpetit.fr :: theme not previously defined.");
	changeClass("icon-toggle", "fa-spinner", "fa-sun");
}
// dark mode button
btn.addEventListener("click", function () {
	// toggles icon
	var stateLoaded = document.getElementById("icon-toggle");
	var loadedTheme = (stateLoaded.classList.contains("fa-sun") ? "light" : "dark");
	if (loadedTheme == "light") {
		console.log("fredericpetit.fr :: theme changed to dark.");
		changeClass("icon-toggle", "fa-sun", "fa-moon");
	} else if (loadedTheme == "dark") {
		console.log("fredericpetit.fr :: theme changed to light.");
		changeClass("icon-toggle", "fa-moon", "fa-sun");
	}
	// toggles theme
	if (prefersDarkScheme.matches) {
		document.body.classList.toggle("light-theme");
		var theme = (document.body.classList.contains("light-theme") ? "light" : "dark");
	} else {
		document.body.classList.toggle("dark-theme");
		var theme = (document.body.classList.contains("dark-theme") ? "dark" : "light");
	}
	localStorage.setItem("theme", theme);
});

// #################### posts dates ####################
function getCompareDate() {
	const d = new Date();
	let month = (d.getMonth() + 1);
	let day = d.getDate();
	let year = d.getFullYear();
	if (month.length < 2) {
		month = "0" + month;
	}
	if (day.length < 2) {
		day = "0" + day;
	}
	return [year, month, day].join("");
};
var forEach = function (array, callback, scope) {
	for (var i = 0; i < array.length; i++) {
		callback.call(scope, i, array[i]);
	}
};
var myNodeList = document.querySelectorAll("span[data-date]");
forEach(myNodeList, function (index, value) {
	if (value.getAttribute('data-date') < getCompareDate()) {
		console.log("fredericpetit.fr :: " + value.getAttribute('data-date') + " need a class.");
		value.classList.add("d-none");
	} else {
		console.log("fredericpetit.fr :: " + value.getAttribute('data-date') + " don't need a class.");
	}
});

// #################### table style ######
var tableElement = document.querySelectorAll('table');
tableElement.forEach(function(table) {
	table.classList.add('table', 'table-striped', 'table-bordered');
});

// #################### blockquote style ######
var blockquoteElement = document.querySelectorAll('blockquote');
blockquoteElement.forEach(function(blockquote) {
	blockquote.classList.add('blockquote');
});