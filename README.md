# fredericpetit.fr website.

_Powered by Jekyll._

| parameter | value |
| ------ | ------ |
| **URL git fredericpetit** | [gitlab.com/fredericpetit/fredericpetit.gitlab.io](https://gitlab.com/fredericpetit/fredericpetit.gitlab.io/) |
| **SCHEDULE** (pipeline) | <spand id="schedule">At 10:00 on Monday, Thursday, & Saturday.</span> |
