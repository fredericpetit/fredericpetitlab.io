---
layout:		post
title:		"Kolab"
date:		2023-06-10 09:00:00 +0200
categories:	tutoriel
author:		"Frédéric"
---

## Procédure d'installation & configuration du groupware Kolab (Centos9 Stream).
_Joyeusement rédigé par l'équipe Kolab (Frédéric Petit - Cédric Verion - Maël Chupin)._

Cette procédure a été testée et dernièrement vérifiée le 10/06/2023 sur CentOS 9 Stream. À noter que l'installation de Kolab ne peut aller au bout sur les distributions GNU/Linux Debian 11 et + et Ubuntu 22.04 et + : divers problèmes de dépendances non satisfaites (manquantes ou versions non compatibles) rencontrées avec le paquet kolab lui-même fourni par kolabsys, un bug report a été envoyé.

Vous pouvez suivre la procédure manuelle ci-dessous ou adapter [cette fiche JSON de configuration](https://gitlab.com/fredericpetit/overdeploy-datas-example/-/blob/main/src/linux/server/centos_9_kolab.json) pour la passer dans mon script de customisation GNU/Linux [Overdeploy](https://gitlab.com/fredericpetit/overdeploy).

Kolab fournira les services suivants :
- panel cockpit (https://192.168.0.76:9090/)
- panel kolab (http://192.168.0.76/kolab-webadmin/)
- échange de fichiers chwala (http://192.168.0.76/chwala/)
- webmail (http://192.168.0.76/roundcubemail/)

### I) Ajouter les bonnes sources.

#### a) dépots natifs "crb" et "extras".
- `dnf config-manager --set-enabled crb`
- `dnf install epel-release epel-next-release`

#### b) dépôt externe mariadb.
- `nano /etc/yum.repos.d/mariadb.repo`

```
[MariaDB]
name = MariaDB
baseurl = https://mirrors.ircam.fr/pub/mariadb/yum/11.0/centos/$releasever/$basearch
gpgkey = https://mirrors.ircam.fr/pub/mariadb/yum/RPM-GPG-KEY-MariaDB
gpgcheck=1
enabled=1"
```

#### c) dépôt externe kolab.
- `nano /etc/yum.repos.d/kolab.repo`

```
[Kolab_16]
name=Kolab 16: Stable Release (CentOS_9)
type=rpm-md
baseurl=http://obs.kolabsys.com/repositories/Kolab:/16/CentOS_9_Stream/
gpgkey = https://ssl.kolabsys.com/community.asc
gpgcheck=1
enabled=1
priority = 60
```

### II) Désactiver SELinux.
Kolab recommande explicitement la désactivation de SELinux, qui nécessiterait un paramètrage plus fin pour autoriser les communications du panel web et du serveur IMAP Cyrus.

Passer à "disabled" la valeur présente pour son status dans :
- `nano /etc/selinux/config`
- `nano /etc/sysconfig/selinux`

Vérifier avec : `sestatus`.

### III) Configurer le réseau (Network Manager).
- lister les interfaces présentes : `ls /etc/NetworkManager/system-connections`

Exemple pour une interface nommée "ens192" avec sa config voulue :
- `nmcli connection modify ens192 ipv4.addresses 192.168.10.8`
- `nmcli connection modify ens192 ipv4.gateway 192.168.10.1`
- `nmcli connection modify ens192 ipv4.dns 8.8.8.8`
- `nmcli connection modify ens192 ipv4.method manual`
- vérifier netmask : (/24 au lieu de /32 par exemple)

Ajouter un FQDN :
- si besoin avant : `hostnamectl set-hostname kolab`
- nano /etc/hosts : `192.168.10.8   kolab.groupware.intra kolab`

### IV) Installer Kolab.
- `dnf install kolab kolab-webadmin -y`

### III) Configurer le pare-feu.
- `firewall-cmd --permanent --add-service=http`
- `firewall-cmd --permanent --add-service=https`
- `firewall-cmd --permanent --add-service=pop3s`
- `firewall-cmd --permanent --add-service=imaps`
- `firewall-cmd --permanent --add-service=smtp`
- `firewall-cmd --permanent --add-service=ldap`
- `firewall-cmd --permanent --add-service=ldaps`
- `firewall-cmd --permanent --add-port=110/tcp`
- `firewall-cmd --permanent --add-port=143/tcp`
- `firewall-cmd --permanent --add-port=587/tcp`
- `systemctl restart sshd`

### V) Configurations.
#### a) Kolab.
- `setup-kolab` **OU** en version avec réponses pré-définies : `setup-kolab --default --timezone=Europe/Paris --mysqlhost=localhost --mysqlserver=existing --mysqlrootpw=PASSWORD_ROOT --directory-manager-pwd=PASSWORD_MANAGER`
- `systemctl enable --now cockpit.socket`

### b) par élément.

{:.table .table-dark}
| élément | fichier de config | logs | nom de service (systemctl) |
| ------ | ------ | ------ | ------ |
| kolab | /etc/kolab/kolab.conf |        |        |
| chwala | | /var/log/chwala/errors.log |        |
| cyrus | /etc/cyrus.conf |        | cyrus-imapd |
| imap | /etc/imapd.conf |        |        |
| guam | /etc/guam/sys.config |        |        |
| roundcube | /etc/roundcubemail/config.inc.php | /var/log/roundcubemail/errors.log |        |

- si besoin : `openssl verify -CAfile cyrus-imapd-ca.pem cyrus-imapd.pem`
- changer "localhost" et "tls" par l'adresse IP locale statique et "ssl" dans kolab, cyrus et roundcube.

### To-Do : storage.
À ce stade l'installation est complète avec tous les logins actifs, mais une erreur "storage error" est présente.