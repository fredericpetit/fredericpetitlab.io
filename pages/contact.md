---
title: Contact
title_page: Contact

permalink: /contact.html
layout: default
---

## Gitlab.

Vous pouvez suivre le développement de mes projets sur mon Gitlab : [https://gitlab.com/fredericpetit](https://gitlab.com/fredericpetit).

## Linkedin.

Retrouvez mon profil Linkedin à cette adresse : [https://www.linkedin.com/in/fredericpetit/](https://www.linkedin.com/in/fredericpetit/).

## Mail.

Si vous souhaitez me contacter par email, utilisez l'adresse &lt;contact AT fredericpetit.fr&gt; .