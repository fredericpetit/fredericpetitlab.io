---
title: Bug Report
title_page: Bug Report

permalink: /bugreport.html
layout: default
---

## github.com

- 22/06/2022 - Songrec : https://github.com/marin-m/SongRec/issues/102
- 26/07/2022 - GNOME extension-list : https://github.com/tuberry/extension-list/issues/11
- 12/10/2022 - Ping Castle : https://github.com/vletoux/pingcastle/issues/150
- 14/03/2023 - jqplay : https://github.com/owenthereal/jqplay/issues/137
- 14/03/2023 - etherpad : https://github.com/ether/etherpad-lite/issues/5701
- 05/04/2023 - GPU-Viewer : https://github.com/arunsivaramanneo/GPU-Viewer/issues/70