# Dictionnaire des notions en configuration Réseau.

## Adresse IP.

Votre adresse IP. Il s'agit d'une adresse unique attribuée à la machine, sous forme de quatre nombre décimaux séparés par des points, comme par exemple 128.253.153.54. Votre administrateur réseau pourra vous la fournir.
Si vous ne configurez que votre système local, c'est à dire une machine isolée du monde (sans Ethernet, SLIP ou PPP), ne faisant que des connexions TCP/IP sur elle-même (loopback), alors votre adresse IP est par convention 127.0.0.1.

## Masque.

Votre masque réseau (netmask). C'est un nombre similaire à votre adresse IP, qui détermine quelle portion de l'adresse spécifie le sous-réseau, et quelle portion spécifie la machine (host) sur ce sous-réseau. (Si ce jargon TCP/IP vous affole, nous vous suggérons de lire un ouvrage quelconque d'initiation à l'administration réseau). Le masque réseau est un ensemble de bits, qui en fonction d'une adresse de votre réseau, permet de déterminer à quel sous-réseau appartient cette adresse. Ceci est très important pour le routage; si par exemple vous vous apercevez que vous pouvez vous connecter à l'extérieur de votre réseau, mais pas à certaines machines de votre réseau local, il y a de grandes chances que votre masque réseau soit faux.
Vos administrateurs réseau ont choisi le masque lors de l'installation du réseau, par conséquent ils doivent être capable de vous fournir la valeur correcte à utiliser. La plupart des réseaux sont des sous-réseaux de classe C, qui utilisent un masque de 255.255.255.0. D'autres réseaux de classe B utiliseront 255.255.0.0; Linux sélectionnera automatiquement un masque par défaut ne considérant aucun sous-réseau si vous ne lui précisez rien.
Tout ceci s'applique également au port loopback: comme son adresse IP est 127.0.0.1, le masque réseau est toujours 255.0.0.0. Vous pouvez soit le préciser explicitement, soit faire confiance au masque par défaut attribué par Linux.

Calculer un masque : https://www.it-connect.fr/adresses-ipv4-et-le-calcul-des-masques-de-sous-reseaux/.

## Adresse Réseau.

Votre adresse réseau. Il s'agit de votre adresse IP masquée bit à bit par un ET logique avec le masque réseau. Par exemple, si votre masque est 255.255.255.0, et votre adresse IP 128.253.154.32, alors votre adresse réseau vaut 128.253.154.0. Si le masque était 255.255.0.0, elle vaudrait 128.253.0.0.
Si vous êtes isolé, sur 127.0.0.1, vous n'avez pas d'adresse réseau.

## Adresse Broadcast.

Votre adresse broadcast. Elle est utilisée pour diffuser des paquets sur toutes les machines de votre sous-réseau. Par conséquent, si l'adresse de votre machine est déterminée par le dernier octet de l'adresse IP (masque réseau 55.255.255.0), votre adresse broadcast sera le résultat d'un OU entre votre adresse réseau et 0.0.0.255.
Par exemple, si votre adresse IP est 128.253.154.32, et votre masque réseau vaut 255.255.255.0, votre adresse de broadcast est alors 128.253.154.255.
Notez que d'anciennes configurations réseau utilisent l'adresse réseau comme adresse de broadcast. Si vous avez un doute, demandez à votre administrateur réseau (dans la plupart des cas il suffit de copier la configuration réseau d'une autre machine du sous-réseau et de remplacer l'adresse IP bien sûr).
Si vous êtes isolé, sur 127.0.0.1, vous n'avez pas d'adresse broadcast.

## Adresse Passerelle.

L'adresse de la passerelle. Il s'agit de l'adresse de la machine qui est votre passerelle vers le monde extérieur (par exemple les machines qui ne sont pas sur votre sous-réseau). Très souvent la machine passerelle a une adresse IP identique à la vôtre mais se terminant par .1; par exemple si votre adresse est 128.253.154.32, la passerelle pourrait être 128.253.154.1. Vos administrateurs réseaux vous fourniront cette adresse.
En fait, vous pouvez avoir plusieurs passerelles. Une passerelle (gateway) est simplement une machine connectée à plusieurs réseaux (elle a des adresses IP sur différents sous-réseaux), et qui route les paquets entre eux. Beaucoup de réseaux ont une seule passerelle vers le monde extérieur (le réseau à côté du vôtre), mais dans certains cas il peut être nécessaire d'accéder à plusieurs autres réseaux.
Si vous ne sortez pas de votre réseau local, vous n'avez pas de passerelle, vous n'en avez pas non plus sur 127.0.0.1 bien sûr.

## Adresse DNS.

L'adresse du serveur de noms. La plupart des machines d'un réseau important utilisent un serveur de noms, qui tranforme les noms des machines en adresses IP. Votre administrateur réseau vous indiquera l'adresse de la machine serveur de noms; vous pouvez aussi avoir un tel serveur localement sur votre système en utilisant le programme named, dans ce cas il aura pour adresse 127.0.0.1. À moins que vous ne deviez absolument utiliser votre propre serveur de noms, nous vous suggérons d'utiliser celui fourni par votre réseau (s'il existe, ce n'est pas obligatoire). La configuration de named est très complexe et sort du cadre de ce guide, dont le but est uniquement de vous permettre de vous connecter et d'utiliser le réseau. Vous pourrez toujours plus tard revenir sur la résolution des adresses si vous en ressentez le besoin.
Un serveur DNS a deux rôles : retrouver une adresse IP à partir d'un FQDN (ZRD), et retrouver un FQDN à partir d'une adresse IP (ZRI).
