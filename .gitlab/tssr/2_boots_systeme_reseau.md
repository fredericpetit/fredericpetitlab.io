# Boots.

## I) Boot système.

1. POG - Power OK / Good.
1. POST - Power On Self Test.
1. BIOS - Périphériques.

### Routeur.

1. **Vérification**.
1. **Lancement** - **Chargement**.
1. **Exécution**.

## II) "Boot" d'un paquet réseau.

1. ET logique entre IP et son masque.
1. Check : table de routage (si pas dans le réseau).
1. Check : table ARP.
1. Envoi d'une ARP Request si inconnu (enregistrée dans table CAM par le switch) + Réception d'une ARP Reply.
1. Informations par la Box qui fait rôle de routeur.
1. Arrive sur le serveur.

## Annexe.

### Sources tierces.

- page _wikipédia_ : https://fr.wikipedia.org/wiki/D%C3%A9marrage_d%27un_ordinateur
- article _dell_ : https://www.dell.com/support/kbdoc/fr-ed/000128270/proc%c3%a9dures-post-et-de-d%c3%a9marrage
