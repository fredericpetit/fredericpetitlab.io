# Câblage, NAS / SAN & Clusterisation.

> RÉSUMÉ : En câblage la catégorie 6 compatible minimum 1 Gb/s est la plus commune, et en stockage mutualisé mode bloc (SAN) le protocole SAN iSCSI est le plus économique pour assurer une redondance et un équilibrage de charge. Le SAN est alors la target, à travers le protocole TCP/IP et les disques virtuels sont remontés sur les serveurs. Pour ce qu'il s'agit d'un cluster, un HA sera définit par son four-9.

# I) Définition.

## A) Câblage.

### 1) Types.

- Câbles **droits** pour équipements différents (commun).
- Câbles **croisés** pour récepteurs identiques sans réseau.

### 2) Capacités.

- Cat. 3 : 10BaseT.
- Cat. 5 : 10BaseT, 100BaseTX.
- Cat. 5E : 10BaseT, 100BaseTX, 1000Base-T.
- Cat. 6 : 1000Base-T.
- Cat. 6A : 10GBase-T (10 000BaseT).
- Cat. 7 : 10GBase-T (10 000BaseT).

| |
| ------ |
| ![CABLES](assets/img/cables_debit2.png){width=600px} |

## B) NAS/SAN/DAS/Cloud.

| type | fonction | connectivité |
| ------ | ------ | ------ |
| **NAS**,<br/>Network Attached Storage | Stockage en **mode fichier**, tout-en-un, via SMB/NFS. | Ethernet |
| **SAN**,<br />Storage Area Network | Stockage en **mode bloc**, mutualisé et très rapide, en baies - fonctions cloisonnement zoning (VLAN-like), découpage LUN (Logical Unit Number), consommation réelle thin provisionning, tiering par types. | SCSI Fiber Channel, SCSI Fiber Channel over Ethernet, iSCSI (Internet Small Computer System Interface) |
| **DAS**,<br />Direct Attached Storage | Stockage en **mode bloc**, non optimisé et directement relié au serveur. | |
| **Cloud** | Stockage externalisé. | |

| | |
| ------ | ------ |
| ![NASSAN2](assets/img/nas_san2.png){width=600px} | ![NASSAN1](assets/img/nas_san1.png){width=600px} |

## II) Clusteurisation.

| type | fonction |
| ------ | ------ |
| **DRS/DS** | Distribué avec pool de ressources et migration automatique si besoin. |
| **HA** | Haute Disponibilité avec infrastructure conséquente. |

Le PRA (Plan de Reprise d'Activité) permet de prévoir un retour à la normale selon le cluster choisit.

## Annexe.

### Sources tierces.

- vidéo _it-connect_ : https://www.youtube.com/watch?v=m-_RPOcU19c
- vidéo _it learning_ : https://www.youtube.com/watch?v=TP793g-TVbw
- article _community.fs.com_ : https://community.fs.com/fr/article/iscsi-storage-basics-plan-iscsi-san.html
