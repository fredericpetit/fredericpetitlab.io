# Calcul de débit.

| unité octet | valeur | unité bit |
| ------ | ------ | ------ |
| 1 octet | | 8 bits |
| 1 Ko | 1024 o | 8 192 bits |
| 1 Mo | 1024 Ko | 8 192 000 bits |
| 1 Go | 1024 Mo | 8 192 000 000 bits |
| 1 To | 1024 Go | 8 192 000 000 000 bits |

- Octet vers Bits = x8
- Bits vers Octets = /8

## I) Formule.

- vitesse = distance / temps
- débit = données / temps
- fréquence d'échantillonage multimédia = échantillons / temps

## II) Exemple 1, on calcule le temps écoulé d'après un débit connu.

### données
- data = 500 Go
- débit = 4 Mo/s
### problème

- on convertit Go to Mo, puis vers Bits : (500 x 1000) * 8 = on a 4 000 000 Bits à transférer
- on divise par le débit : 4000000 bits * 1 / 4 = en 1000 secondes (/60 = 166 min)
- **Il faudra 166 minutes pour transférer 500 Go à 4 Mo/s.**

## III) Exemple 2, on calcule le débit d'après un temps donné.

### données
- data = 4,5 Go
- débit = 4 Mb/s
- temps voulu = 2h

### problème
- on convertit Go to Mo, puis vers Bits : (4.5 x 1000) * 8 = on a 36 000 Bits à transférer
- on divise par le débit : 36000 bits * 1 / 4 = 9 000 Mb
- on calcule le temps : 9000 / 60 = 150 min (ou 150 / 60 = 2h30)
- **Pour transférer en 2h il faudra un débit de 36000 bits / 7200 (2h) = 5 Mb/s.**

## Annexe.

### Sources tierces.

- article _google_ sur le streaming : https://support.google.com/youtube/answer/2853702?hl=fr
