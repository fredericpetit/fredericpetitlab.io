# Réseau OS.

## I) GNU/Linux.

## Lister interfaces réseau.
- /sys/class/net/

## Modifier réseau.
- interfaces : /etc/network/interfaces
- networkmanager : /etc/NetworkManager/system-connections
- netplan : /etc/netplan

## DNS.
- /etc/resolv.conf (reload automatique)

## II) Microsoft Windows.

Graphique ou PowerShell.
