# Réseau Active Directory.

> RESUME : Roaming synchronisé et Home mappé.

## I) Répertoires Netlogon et Sysvol.

| dossier | fonction |
| ------ | ------ |
| **sysvol** | Stocke les fichiers publics d'un domaine (schéma d’annuaire), qui sont répliqués sur chaque contrôleur de domaine. |
| **netlogon** | Stocke des scripts d'ouverture de session et des stratégies de groupe qui peuvent être utilisés par les ordinateurs déployés dans un domaine. |

## II) Profil itinérant et dossier de base.

| type | fonction |
| ------ | ------ |
| **profil itinérant**<br />(roaming) | Dossier partagé, **synchronisé** avec échange de données, accessible partout et de la forme `\\FQDN\REPERTOIRE_PROFILS\%username%`. |
| **dossier de base**<br />(home) | Dossier partagé, **mappé** sur un lecteur réseau, accessible partout. |

## Annexe.

### Sources tierces.

- article _It-connect_ : https://www.it-connect.fr/active-directory-creer-des-profils-itinerants-pour-ses-utilisateurs/
