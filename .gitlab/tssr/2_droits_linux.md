# Droits sous GNU/Linux.

## I) Droits des utilisateurs.

### A) commandes.

| commande | format |
| ------ | ------ |
| chmod | SRWE |
| chown | user:group |
| chgroup | group |

### B) SRWE.

| spécial | proprio | group | others |
| ------ | ------ | ------ | ------ |
| SetUID exec droits proprio, SetGID exec droits groupe, sticky bit swap.| RWE | RWE | RWE |

### C) RWE numérique.

| droit | valeur |
| ------ | ------ |
| Read | 4 |
| Write | 2 |
| Execute | 1 |

## II) Gestion des utilisateurs.

### A) Interactif.

| add/change | del | fichier |
| ------ | ------ | ------ |
| **adduser** [username] | **userdel** [username] | /etc/passwd |
| **addgroup** [groupname / username groupname] | **groupdel** [username groupname] | /etc/group |

### B) Script.

| add/change | del | fichier |
| ------ | ------ | ------ |
| **useradd** [username] | **userdel** [username] | /etc/passwd |
| **groupadd** [groupname] | **groupdel** [groupname] | /etc/group |
| **usermod** [-G groupname username] | | /etc/group |

## Annexe.

### Sources tierces.

- article _Linuxaddict_ : https://www.linuxaddict.fr/index.php/2018/06/04/les-droits-speciaux-sous-gnu-linux-setuid-setgid-sticky-bit-et-umask/.
