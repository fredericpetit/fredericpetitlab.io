# CPU.

> RÉSUMÉ : Les CPUs 32bits ne savaient gérer que jusqu'à 4Go (2^32) de RAM et avec 8 coeurs maximum, les CPUs 64 bits gérent 16 exaoctets de RAM en théorie. Les **CPUs calculent** les opérations, le nombre de bits du processeur correspondant au nombre de lignes de bus d'adresses à la puissance 2. Le **chipset régule** la vitesse de transfert des données.

## I) AMD.

- Am2900 (1975)
- 29000 (29K) (1987–95)
- Amx86 (1986–95) x86
- K5 (1996) x86
- K6 (1997–2001 x86
- K7 (1999–2005) x86
- K8 (2003–2008) AMD64
- K10 (2007) AMD64
- Bulldozer (2011) AMD64
- Zen (2017) AMD64

## II) Intel.

- Pentium (1993)
- Pentium II (1997)
- Pentium III (1999)
- Pentium 4 (2000)
- Xeon (2001)
- Intel 64 (2001)
- Core (2005)
