# Sauvegardes.

> RÉSUMÉ : La **différentielle différe de la complète** et l'**incrémentielle rajoute au quotidien**.

## I) Définition.

| type | Lundi | Mardi | Mercredi | Total |
| ------ | ------ | ------ | ------ | ------ |
| **complète** | 500 | 510 | 520 | **1520** |
| **différentielle**<br />(vitesse, création/modification par rapport à la complète) | 500 | 10 | 30 | **540** |
| **incrémentielle**<br />(stockage, création/modification par rapport à la veille) | 500 | 10 | 20 | **530** |

NOTE : Les sauvegardes peuvent être individuelle (fichiers) ou en bloc (système).

## II) 3, 2, 1 !
3 copies, 2 supports, 1 externalisé.
