# Traces Ethernet et ARP.

> RÉSUMÉ : 6 oc **@-mac D**, 6 oc **@-mac S**, au 24 le protocole, au 25 le TTL et les **@-ip** à partir du 27.

## I) Trame Ethernet avec Trace de Paquet IP - éléments importants.

| élément | localisation | taille |
| ------ | ------ | ------ |
| **@-mac D** | # 1  - # 6 | 6 octets |
| **@-mac S** | # 7 - # 12 | 6 octets |
| **type IP / version** | # 13 - # 14 | 2 octets |
| **taille paquet** | # 17 - # 18 | 2 octets | 
| **protocole** | # 24 | 1 octet |
| **TTL** | # 25 | 1 octet |
| **@-ip S** | # 27 - # 30 | 4 octets |
| **@-ip D** | # 31 - # 36 | 4 octets |

| |
| ------ |
| ![ip](assets/img/trame_ip.png){width=600px} |

### Notes.
- le type '08 00' correspond à IPv4.
- codes TCP = 0x06, UDP = 0x17, ICMP = 0x01.

## II) Message ARP.

> Le protocole ARP (Address Resolution Protocol) est utilisé par TCP/IP pour mapper une adresse IP de couche 3 à une adresse MAC de couche 2. Lorsqu’une trame est placée sur le réseau, elle doit posséder une adresse MAC de destination. Pour détecter de façon dynamique l’adresse MAC d’un périphérique de destination, une requête ARP est diffusée sur le réseau local. Le périphérique qui contient l’adresse IP de destination répond. Ensuite, l’adresse MAC est consignée dans le cache ARP. Chaque périphérique sur le réseau local conserve son propre cache ARP, ou un petit espace dans la mémoire vive qui contient les résultats d’ARP. Un temporisateur de cache ARP supprime les entrées correspondantes qui n’ont pas été utilisées pendant un certain temps. Les délais diffèrent selon le périphérique utilisé. Par exemple, certains systèmes d’exploitation Windows stockent les entrées de cache ARP pendant 2 minutes. Si l’entrée est de nouveau utilisée au cours de ce délai, le temporisateur ARP de cette entrée est prolongé de 10 minutes. ARP constitue un parfait exemple de compromis de performances. Sans cache, ARP doit constamment demander des traductions d’adresses à chaque placement d’une trame sur le réseau. Ceci ajoute de la latence à la communication et peut encombrer le réseau local. Inversement, des temps d’attente illimités peuvent entraîner des erreurs avec des périphériques qui quittent le réseau ou modifient l’adresse de couche 3.

| |
| ------ |
| ![arp](assets/img/trame_arp.jpg){width=600px} |

## III) Limitations.

Le MTU (Maximum Transmission Unit) est à 1500 par convention.

## Annexe.

### Sources tierces.

- décodeur de paquet IP : https://hpd.gasmi.net/.
- trame ip : https://www.frameip.com/entete-ip/.
- trame arp : https://www.frameip.com/entete-arp/.
- vidéo de _Promethee Spathis_ : https://www.youtube.com/watch?v=6rPzY9kAxRk.
