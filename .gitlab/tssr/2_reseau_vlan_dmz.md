# VLAN & DMZ.

> RÉSUMÉ : VLAN pour segmenter et sécuriser, DMZ pour faire tampon.

## I) VLAN.

### A) Définition.

> Un réseau local virtuel, communément appelé VLAN (pour Virtual LAN), est un réseau informatique logique indépendant. De nombreux VLAN peuvent coexister sur un même commutateur réseau ou « switch ».

src : https://fr.wikipedia.org/wiki/R%C3%A9seau_local_virtuel

> Un VLAN (Virtual Local Area Network ou Virtual LAN, en français Réseau Local Virtuel) est un réseau local regroupant un ensemble de machines de façon logique et non physique. En effet dans un réseau local la communication entre les différentes machines est régie par l'architecture physique. Grâce aux réseaux virtuels (VLANs) il est possible de s'affranchir des limitations de l'architecture physique (contraintes géographiques, contraintes d'adressage, ...) en définissant une segmentation logique (logicielle) basée sur un regroupement de machines grâce à des critères (adresses MAC, numéros de port, protocole, etc.).

src : https://forum.hardware.fr/hfr/WindowsSoftware/quelle-dmz-vlan-sujet_259076_1.htm

> Virtual Local Area Network, c'est un LAN virtuel sur un même réseau local. On peut configurer les VLANs de différentes manières pour que par exemple les stations sur des VLANs différents ne communiquent pas entre eux pour que un VLAN fasse parti de plusieurs VLANs, pour que un VLAN puisse voir un autre VLAN sans que ce dernier voit le premier. Et ainsi de suite...

src : https://forum.hardware.fr/hfr/WindowsSoftware/quelle-dmz-vlan-sujet_259076_1.htm

| |
| ------ |
| ![vlan2](assets/img/vlan2.png){width=600px} |

### B) Utilisation.

| type | fonction |
| ------ | ------ |
| **segmentation** | Séparer les flux et réduire la taille d'un domaine de broadcast. |
| **sécurité** | Permet de créer un ensemble logique isolé pour améliorer la sécurité. Le seul moyen pour communiquer entre des machines appartenant à des VLAN différents est alors de passer par un routeur. |

Par conséquent, les VLAN permettent aussi d'**améliorer la gestion du réseau** et d'**optimiser la bande passante**.

| |
| ------ |
| ![vlan](assets/img/tp_vlan.jpg){width=600px} |

## II) DMZ.

### A) Définition.

> Sur un pare-feu, on appelle DMZ une zone qui n'est ni publique, ni interne. Ces zones ne peuvent exister que si le pare-feu possède plus de deux interfaces réseau, une pour la connexion au réseau externe et l'autre pour la connexion au réseau interne. Si un pare-feu possède trois interfaces au moins (ou trois "pattes" ), il est possible de créer une DMZ sur la troisième interface. On y placera les serveurs qui ont besoin de sortir sur l'extérieur, mais qui ont également besoin d'être protégés des menaces internes. Ainsi, si des utilisateurs en interne veulent faire des opérations frauduleuses sur les serveurs, ils devront franchir la barrière du pare-feu et les règles de filtrage mises en place. En se plaçant du côté extérieur, mettre les serveurs en DMZ permet de protéger le réseau interne des menaces externes, puisqu'un pirate arrivant à accéder en DMZ devra encore fournir un effort pour pénétrer sur le réseau interne.

src : https://forum.hardware.fr/hfr/WindowsSoftware/quelle-dmz-vlan-sujet_259076_1.htm

> C'est une zone dépourvu de règles ou du moins très pauvre en règle de pare-feux, elle est accessible de partout du WAN comme du LAN mais n'a pas accès (de préférence) au LAN.

src : https://forum.hardware.fr/hfr/WindowsSoftware/quelle-dmz-vlan-sujet_259076_1.htm

## Annexe.

### Sources tierces.

- article _FITN_ : https://www.fingerinthenet.com/vlan/ et sa vidéo https://www.youtube.com/watch?v=nsv28gyZozU
- tutoiel _Cisco Packet Tracer FR - VLAN_ : https://www.youtube.com/watch?v=ze5uAhy0ETI
