# Réseau Cloud.

> RÉSUMÉ : SaaS > PaaS > IaaS dans l'ordre - de services à + de services.

## I) Définition.

| type | fonction |
| ------ | ------ |
| **public** | Externe à l'entreprise et partagé entre plusieurs entités, accès via internet, paiement de type "pay as you go" (abonnement pour les entreprises). Il est géré par un prestataire externe propriétaire des infrastructures, avec des ressources partagées entre plusieurs sociétés. |
| **privé** | Structure interne à l'entreprise ou à un groupement d'entreprises et complètement dédié en accès sécurisé sur internet mutualisé entre les différentes entités d'une seule et même entreprise.  |
| **hybride** | Conjonction des 2 types. |

### II) Services.

| nom | fonction |
| ------ | ------ |
| **SaaS** | Application précise, pour **Utilisateurs finaux** - ex : Office 365, Google Maps. |
| **PaaS** | OS précis ou base de données, pour **Développeurs** - ex : Active Directory, MySQL. |
| **IaaS** | Machine totale, pour **Administrateurs** - ex : Microsoft, Amazon. |

| |
| ------ |
| ![CERTRDS](assets/img/cloud.jpg){width=600px} |

## Annexe.

### Sources tierces.

- vidéo _cookie connecté_ : https://www.youtube.com/watch?v=Al-E4C69UmQ
