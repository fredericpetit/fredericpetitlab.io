# Droits (permissions / autorisations) sous Microsoft Windows.

## I) NTFS.

1. Ouvrez **l’onglet « Sécurité »**.
1. Dans la boîte de dialogue du dossier « Propriétés », cliquez sur « Modifier ».
1. Cliquez sur le nom de l’objet dont vous souhaitez modifier les autorisations.
1. Sélectionnez « Autoriser » ou « Refuser » pour chacun des paramètres.
1. Pour appliquer les autorisations, cliquez sur « Appliquer ».

## II) Partage.

1. Cliquez avec le bouton droit sur **le dossier partagé**.
1. Cliquez sur « Propriétés ».
1. Ouvrez **l’onglet « Partage »**.
1. Cliquez sur « Partage avancé ».
1. Cliquez sur « Autorisations ».
1. Sélectionnez un utilisateur ou un groupe dans la liste.
1. Sélectionnez « Autoriser » ou « Refuser » pour chacun des paramètres.

## III) NTFS vs Partage.
Les droits de partage s'appliquent uniquement au dossier partagé, tandis que les droits NTFS peuvent être finement règlés et sont à préférer.

## Annexe.

### Sources tierces.

- article _Netwrix_ : https://blog.netwrix.fr/2019/02/28/differences-entre-autorisations-de-partage-et-autorisations-ntfs/.
- article _It-connect_ : https://www.it-connect.fr/serveur-de-fichiers-les-permissions-ntfs-et-de-partage/.
