## I) Définition.

SNMP (Simple Network Management Protocol) pour monitorer des équipements et alerter sur la moindre défaillance, afin de :

- Réduire les périodes d’indisponibilité.
- Fournir à tout moment l’état du réseau et de ses composants.
- Inventorier les équipements.
- Quantifier et optimise les trafics.
- Faciliter la planification des interventions sur le réseau.
- Permettre une automatisation des procédures de correction des problèmes.

## II) Composition.

- serveur avec **MIB (Managed Information Base)**.
- client(s) avec **OID (Objectfs IDentifier)**.

## II) Fonctionnalités.

| type | vérification | port |
| ------ | ------ | ------ |
| polling | active, régulière par script | UDP 161 |
| trap | passive, sur alerte | UDP 162 |
