# Virtualisation.

## Modes.

> RÉSUMÉ : Le BRIDGE simule, le NAT masque, l'HOST-ONLY réserve, le LAN SEGEMENT isole.

| vmware | virtualbox |
| ------ | ------ |
| ![récap](assets/img/VMware-Workstation-Pro-Recapitulatif-connexion-reseau.png){width=600px} | ![récap](assets/img/virtualbox-synthese-modes-reseau.png){width=600px} |

| mode | illustration |
| ------ | ------ |
| NAT, avec traduction - carte réseau virtuelle | ![nat](assets/img/VMware-Workstation-Pro-Schema-NAT.png){width=600px} |
| BRIDGE PONT, à poil - carte réseau physique | ![bridge](assets/img/VMware-Workstation-Pro-Schema-Bridged.png){width=600px} |
| HOST ONLY - carte réseau virtuelle | ![host](assets/img/VMware-Workstation-Pro-Schema-Host-Only.png){width=600px} |
| LAN SEGMENT - carte réseau virtuelle | ![host](assets/img/VMware-Workstation-Pro-Schema-LAN-segment.png){width=600px} |
