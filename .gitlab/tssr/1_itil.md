# ITIL.

> RÉSUMÉ : ITIL est la gestion des services, avec 3 niveaux de support, pour une organisation optimale.

## I) Objectif.
Ouvrir, Qualifier, Traiter, Clôturer.

## II) SLA.
Service Level Agrement = contrat.

## III) Service Opération - Exploitation.

### Sous-processus de gestion.

| service | fonction |
| ------ | ------ |
| infra immobilière | planifier, services généraux |
| infra informatique | serveurs, postes, réseau, sécurité |
| applications | commerciales ou maison (no dév, no code) |
| accès | droits utilisateurs et ressources |
| service desk | point de contact unique entre utilisateurs et service informatique |
| évènements | catégorisation, priorisation, traitement (router) - support Niveau 1 |
| incident(s) (isolé) | qualifier et rétablir le service dans le respect des SLA - support Niveau 2 |
| problème(s) (global) | identifier cause racine de l'incident, solutionner directement ou workaround - support Niveau 3 |
| requêtes | automatisations |
| changements | évaluer bénéfices/risques par le comité CAB ou ECAB (experts métiers + experts techniques) |

## IV) Niveau de support.

| niveau | composition |
| ------ | ------ |
| 1 | techniciens TAI |
| 2 | spécialistes TSSR |
| 3 | experts TSSR, éditeur ou fournisseur |

## V) Conduite de support.

1. contact
1. identifier utilisateur / matériel
1. identifier incident / qualifier / reformuler
1. avoir un plan / s'organiser
1. traitement (direct ou différé)
1. fin
