# Tables ARP / CAM / ROUTAGE.

> RÉSUMÉ : Chaque équipement posséde sa table ARP, les SWITCHS ont une table CAM, et les ROUTEURS ont une table de ROUTAGE.

## I) Définition.

| table | éléments | équipement |
| ------ | ------ | ------ |
| ARP | **@-ip** + **@-mac** + type (statique ou dynamique) | tous |
| CAM | port (du switch) + **@-mac** | switch |
| Routage | **@-res** + **@-mas** + **@-pass** + **@-int** | routeur |

| | |
| ------ | ------ |
| ![table](assets/img/table_arp.png){width=600px} | |
| ![table](assets/img/table_cam.png){width=600px} | ![table](assets/img/table_routage.png){width=600px} |

## II) Notes.

- L'**@-ip** est chargée avec l'OS tandis que l'**@-mac** est chargée dès le BIOS.
- Dans une ARP Request l'**@-mac** source est la passerelle.
- Dans une table de routage, pour joindre **@-res** de masque **@-mas** je passe par **@-pass** via **@-int**, et l'adresse **@-pass** doit toujours être dans le même réseau que son **@-int**.

## Annexe.

### Sources tierces.

- vidéo de _Prométhée Spathis_ : https://www.youtube.com/watch?v=ivAuTsFXByo.