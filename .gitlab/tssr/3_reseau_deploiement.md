# Déploiement Réseau.

> RÉSUMÉ : Pour masteriser une machine Windows on effectue une capture de l’état actuel du système, puis dans la console de gestion du Windows Server on récupère par le réseau cette image pour la mettre en déploiement, en considérant une diffusion via WDS et un paramétrage fin via MDT.

## I) Définition.

### A) (i)PXE/WDS/MDT.

| type | outils |
| ------ | ------ |
| **image** | Clonezilla. |
| **full-touch**, interactif  | WDS (_boot.wim_ + _install.wim_ originale). |
| **lite-touch**, semi-interactif | WDS + MDT (_boot.wim_ + _custom.wim_ modifiée). |
| **zero-touch**, non interactif | System Center Configuration Manager. |

### B) RDS.

1. disposer d'applications lancées avec les ressources du serveur.
1. disposer d'un environnement de bureau complet depuis le serveur.

## II) Procédure.

### A) WDS/MDT.

1. Boot réseau avec adresse fourni par le DHCP du WDS.
1. WinPE chargé en RAM.
1. Requête du client qui récupére une image à installer.

### B) MDT.

| phase | fichier |
| ------ | ------ |
| **avant** | _CustomSettings.ini_ + _Bootstrap.ini_. |
| **après** | TASK Sequence. |

## Annexe.

### Sources tierces.

- vidéo _it-connect_ : https://www.youtube.com/watch?v=ILon8Quv924
- vidéo _geek advisor_ : https://www.youtube.com/watch?v=2ZqHxnDZruw
- vidéo _processus thief_ : https://www.youtube.com/watch?v=vj3sio8Drbo
