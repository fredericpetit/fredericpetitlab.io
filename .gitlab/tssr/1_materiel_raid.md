# RAID.

> RÉSUMÉ : Les différents RAID cherchent l'équilibre entre **redondance** et **équilibrage de charge**.

## I) Définition.

| type | 0 | 1 | 5 | 10 | 51 | 
| ------ | ------ | ------ | ------ | ------ | ------ |
| **nature** | stripping | mirroring | parity | double stripping | mirror + parity |
| **objectif** | performance | redondance | perf + redondance | perf + redondance | perf + redondance |
| **explication** | réparti sur 2 disques minimum, si un disque perdu tout est perdu | miroir sur 2 disques minimum | strip avec parité sur 3 disques minimum | double stripping sur 4 disques minimum | miroir de parité sur 6 disques minimum |
| **stockage** | pas de perte | perte 50%  | perte 30% | perte 50% | perte 50% |

Particularités :
- redondance : répéter une information.
- équilibrage de charges : répartir le trafic entre plusieurs serveurs.

| |
| ------ |
| ![raid](assets/img/raid.png){width=600px} |

## II) RAID Logiciel.

### A) GNU/Linux.
- **INSTALLER** le paquet '_mdadm_'.

### B) Microsoft Windows.
- **ALLER** dans '_Gestion des disques_' (**diskmgmt.msc**) > '_initialiser en GPT_'.
- **CHOISIR** '_Nouveau volume agrégé par bandes_' pour RAID 0 ou '_Nouveau volume en miroir_' pour RAID 1.
- **RÉACTIVER** le volume encore fonctionnel pour tenter de retrouver des données après une perte.

## III) RAID Matériel.

- DSM de Synology

## Annexe.

### Sources tierces.
- vidéo GuiPoM : https://www.youtube.com/watch?v=8EdJwxhNFwA
