# Réseau NAT / PAT.

> RÉSUMÉ : Le type de NAT le plus commun aujourd'hui est le dynamique PAT utilisé par exemple sur les boxs internet des particuliers.

## I) Définition.

| type | fonction |
| ------ | ------ |
| **masquage**<br />(nat outbound) | permet à une IP privée de **sortir** sur le net. |
| **port forwarding**<br />(nat inbound) | permet à une IP publique de **rentrer** dans un serveur privé. |

| |
| ------ |
| ![natpat](assets/img/nat_pat1.png){width=600px} |

## II) Différents types de NAT.

| type | fonction |
| ------ | ------ |
| **NAT statique** | une IP publique = une machine connectée sur le réseau local. |
| **NAT dynamique** | une IP publique = une machine connectée sur le réseau local,<br />temporairement et ré-adressable. |
| **NAT dynamique PAT** | une IP publique = plusieurs machines connectées sur le réseau local. |

## Annexe.

### Sources tierces.

- article de _It-connect_ : https://www.it-connect.fr/le-nat-et-le-pat-pour-les-debutants/.
- vidéo de _Cookie connecté_ : https://www.youtube.com/watch?v=jq3SLuhIyPI.
