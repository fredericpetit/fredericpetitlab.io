# Mail.

## I) Définition.

| nom | fonction |
| --- | --- |
| <nobr>**MUA** (Mail **User Agent**)</nobr> | Le **client** de messagerie (KMail, Evolution, etc ...). |
| <nobr>**MTA** (Mail **Transfert Agent**)</nobr> | L'**agent** qui va **transférer** votre mail vers le serveur chargé de la gestion des emails de votre destinataire,<br />dans la pratique le courrier peut transiter par plusieurs MTA. |
| <nobr>**MDA** (Mail **Delivery Agent**)</nobr> | Le **service** de **remise** du courrier dans les boîtes aux lettres des destinataires. |

## II) Protocoles.

| nom | fonction |
| ------ | ------ |
| **POP** | entrant, **stocke** (ports standard 110/995) |
| **IMAP** | entrant, **synchronise** (ports standard 143/993) |
| **SMTP** | sortant, **envoi** (ports standard 25/465) |

## Annexe.

### Sources tierces.

- article _it-connect_ : https://www.it-connect.fr/messagerie-decouverte-des-protocoles-smtp-pop-imap-et-mapi/
