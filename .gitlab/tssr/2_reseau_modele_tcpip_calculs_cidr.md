# Modèle TCP/IP & calculs.

## I) Binaire & Hexa.

### A) Binaire avec puissances de 2.

#### 1) Communs.

| 128<br />(2^7) | 64<br />(2^6) | 32<br />(2^5) | 16<br />(2^4) | 8<br />(2^3) | 4<br />(2^2) | 2<br />(2^1) | 1<br />(2^0) | |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
|1|1|1|1|1|1|1|1| 255 |
|1|1|1|        |        |        |        |        | 224 |
|1|1|        |        |        |        |        |        | 192 |
|1|        |        |        |        |        |        |        | 128 |
|1|        |1|1|        |        |        |        | 176 |
|1|        |1||        |        |        |        | 160 |

### 2) Masque.

| 128<br />(2^7) | 64<br />(2^6) | 32<br />(2^5) | 16<br />(2^4) | 8<br />(2^3) | 4<br />(2^2) | 2<br />(2^1) | 1<br />(2^0) | |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
||||||||| 0 |
|1|||||||| 128 |
|1|1||||||| 192 |
|1|1|1|||||| 224 |
|1|1|1|1||||| 240 |
|1|1|1|1|1|||| 248 |
|1|1|1|1|1|1||| 252 |
|1|1|1|1|1|1|1|| 254 |
|1|1|1|1|1|1|1|1| 255 |

### B) Hexadécimal.

| décimal | hexa | hexa 1 octet (x16 + ..) |
| ------ | ------ | ------ |
| 0 | 0 | 0 + .. |
| 1 | 1 | 16 + .. |
| 2 | 2 | 32 + .. |
| 3 | 3 | 48 + .. |
| 4 | 4 | 64 + .. |
| 5 | 5 | 80 + .. |
| 6 | 6 | 96 + .. |
| 7 | 7 | 112 + .. |
| 8 | 8 | 128 + .. |
| 9 | 9 | 144 + .. |
| 10 | A | 160 + .. |
| 11 | B | 176 + .. |
| 12 | C | 192 + .. |
| 13 | D | 208 + .. |
| 14 | E | 224 + .. |
| 15 | F | 240 + .. |

## II) Classes d'IP.

### A) Définition.

> La suite des protocoles Internet est l'ensemble des protocoles utilisés pour le transfert des données sur Internet. Elle est aussi appelée suite TCP/IP, DoD Standard (DoD pour Department of Defense) ou bien DoD Model ou encore DoD TCP/IP ou US DoD Model. Elle est souvent appelée TCP/IP, d'après le nom de ses deux premiers protocoles : TCP (de l'anglais Transmission Control Protocol) et IP (de l'anglais Internet Protocol). Ils ont été inventés par Vinton G. Cerf et Bob Kahn, travaillant alors pour la DARPA, avec des influences des travaux de Louis Pouzin. Le document de référence est RFC 1122.

src : https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet

> La distinction entre les adresses de classe A, B ou C a été ainsi rendue obsolète, de sorte que la totalité de l'espace d'adressage unicast puisse être gérée comme une collection unique de sous-réseaux indépendamment de la notion de classe. Le masque de sous-réseau ne peut plus être déduit de l'adresse IP elle-même, les protocoles de routage compatibles avec CIDR, dits classless, doivent donc accompagner les adresses du masque correspondant. C'est le cas de Border Gateway Protocol dans sa version 4, utilisé sur Internet (RFC 1654 A Border Gateway Protocol 4, 1994), OSPF, EIGRP ou RIPv2. Les registres Internet régionaux (RIR) adaptent leur politique d'attribution des adresses en conséquence de ce changement. L'utilisation de masque de longueur variable (Variable-Length Subnet Mask, VLSM) permet le découpage de l'espace d'adressage en blocs de taille variable, permettant une utilisation plus efficace de l'espace d'adressage.

src : https://fr.wikipedia.org/wiki/Adresse_IP#Agr%C3%A9gation_des_adresses

### B) Listing.

| classe | plage | bit(s) de départ dans l'IP | masque par défaut<br />(traduire en binaire)<br />+ CIDR<br />(par rapport au nbr de bits à 1 dans masque à partir de la gauche et en continu) | max. réseaux<br />NET-ID<br />(par rapport au masque) | max. machines<br />HOST-ID<br />(par rapport au masque) | privé |
| ------ | ------ | ------ |  ------ | ------ | ------ | ------ | 
| A<br />(128) | **0**.0.0.0 à<br />**127**.255.255.255 | 0 | **255**.0.0.0<br />1 octet à 255<br />= 8 bits à 1<br />= **/8** | bit de départ à 1,<br/>suivi de 7 bits à 0<br />= 2^7<br />= 128 | 24 bits à 0<br />= 2^24<br />= 16 777 214 (-2) | 10.x.x.x,<br />127.x.x.x |
| B<br />(64) | **128**.0.0.0 à<br />**191**.255.255.255 | 10 | **255.255**.0.0<br />2 octets à 255<br />= 16 bits à 1<br />= **/16** | bit de départ à 10,<br/>suivi de 14 bits à 0<br /> = 2^14<br />= 16 384 | 16 bits à 0<br />= 2^16<br />= 65 534 (-2) | 172.16.x.x à 172.31.x.x |
| C<br />(32) | **192**.0.0.0 à<br />**223**.255.255.255 | 110 | **255.255.255**.0<br />3 octets à 255<br />= 24 bits à 1<br />= **/24** | bit de départ à 110,<br/>suivi de 21 bits à 0<br /> = 2^21<br />= 2 millions | 8 bits à 0<br />= 2^8<br />= 254 (-2) | 192.168.x.x |
| D<br />(16) | **224**.0.0.0 à<br />**239**.255.255.255 | 1110 | **255.255.255.255**<br />4 octets à 255<br />= 32 bits à 1<br />= **/32** | x | x | x |
| E<br />(16) | **240**.0.0.0 à<br />**255**.255.255.255 | 1111 | x | x | x | x |
| 256 | | | | | | |

## III) CIDR.

| cidr1 | cidr2 |
| ------ | ------ |
| ![cidr1](assets/img/cidr1.png){width=600px} | ![cidr2](assets/img/cidr2.png){width=600px} |

## IV) Calcul masque de sous-réseau.

### A) Méthode full binaire (@-ip = 192.168.0.10 / 24).

| type | adresse | binaire |
| ------ | ------ | ------ |
| **adresse IPv4** | 192.168.0.10 | 11000000 10101000 00000000 00001010 |
| **masque de sous-réseau** | 255.255.255.0 | 11111111 11111111 11111111 00000000 |
| **adresse réseau** | 192.168.0.0 | 11000000 10101000 00000000 00000000  |
| **hôtes disponibles** | 8 bits à zéro dans le masque = 2^8 = 256-2 = 254 | 00000000 |

### B) Méthode perso (@-ip = 100.191.6.33 / 13).

#### @-res.
1. traduction par blocs de 8 du **@-mas** et de **@-ip**
2. note des 2^ des bits à 0 dans l’octet de travail du **@-mas**
3. octets de travail **@-ip** / **@-mas** : 1011-1111 / 1111-1000 = 1011-1000 = 184
4. **@-res** = 100.184.0.0

#### @-bro.
1. 2^nombre de bits à 0 dans l’octet de travail du **@-mas** = 2³ = 8
2. valeur octet de travail de **@-res** + une plage - 1 = 184 + 8 - 1
3. **@-bro** = 100.191.255.255

## VI) Découpages.

### A) Découpage net-ID.

- Principe : **on rajoute des bits à 1 par rapport au masque donné**.
- Formule : **nombre de sous-réseaux voulu = 2 ^ ?**; nouveau masque = **masque actuel + ?**;

### B) Découpage host-ID.

- Principe : **on rajoute des bits à 0 par rapport à /32**.
- Formule : **nombre de machines voulu = 2 ^ ?**; nouveau masque = **32 - ?**;

## VII) Notes.

**Général**
- octet = byte
- Bit de poids fort le premier, Bit de poids faible le dernier.

**Valeurs**
- 1 octet = 8 bits
- 4 octets (**@-ip** IPV4) = 32 bits
- 6 octets (**@-mac**) = 48 bits
- 16 octets (**@-ip** IPV6) = 128 bits

**Définitions**
- le masque détermine partie réseau (fixe) **255...** et partie hôte (variable) **..0**
- l'adresse réseau finit part 0, jamais attribuable
- l'adresse broadcast finit (par défaut) par 255, jamais attribuable

## Annexe.

### Sources tierces.

- calculateur binaire hexa : https://sebastienguillon.com/test/javascript/convertisseur.html
- article calcul masque : https://www.it-connect.fr/adresses-ipv4-et-le-calcul-des-masques-de-sous-reseaux/
- article calcul masque : https://web.maths.unsw.edu.au/~lafaye/CCM/internet/ip.htm
- article découpage : https://www.inetdoc.net/articles/adressage.ipv4/adressage.ipv4.exercises.html
- vidéo adresse IP et masques de sous-réseaux : https://www.youtube.com/watch?v=RnpSaDSSjR4
- calculateur correcteur réseau masque : https://cric.grenoble.cnrs.fr/Administrateurs/Outils/CalculMasque/
- calculateur correcteur subneting masque : https://www.calculator.net/ip-subnet-calculator.html
- vidéo _cookie connecté_ : https://www.youtube.com/watch?v=26jazyc7VNk
