# VERBATIM.

`Model : 2023-04-13-self.`

| param | value |
| ------ | ------ |
| **license** | Choose license from [spdx.org/licenses](https://spdx.org/licenses/). |
| **tokens** | x |
| **CI/CD variables** | x |
| **CI/CD files** | Create _./.gitlab/default-ci.yml_ & _./.gitlab/extend-ci.yml_, based on '_Self_' home, and _./gitlab-ci.yml_. |
| **Composer** | x |
| **pipeline trigger** | Action "build-deploy" keyword in commit. Source "web" do "build-deploy" action. |
| **pipeline schedule** | "build-deploy" do "build-deploy" action 10h00 on Monday, Thursday, & Saturday. |