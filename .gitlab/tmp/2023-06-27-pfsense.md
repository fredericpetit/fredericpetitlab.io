---
layout:		post
title:		"pfSense"
date:		2023-06-27 09:00:00 +0200
categories:	tmp
author:		"Frédéric"
---

## Procédures pfSense.
Ces procédures ont été testées et dernièrement vérifiées le 27/06/2023.

### I) Procédure pfSense + OpenVPN.

#### A) Installation.

##### 1) Topologie de départ.

<center><img src="../../../../assets/img/tuto/pfsense1.png" alt="pfsense1" /></center><br />

{:.table .table-dark}
| machine | réseau | fonction |
| ------ | ------ | ------ |
| **serveur** pfSense<br />(réseau WAN & LAN) | - Carte 1 WAN : IP 172.16.26.129/24, GW 172.16.26.2, DNS 172.16.26.2<br />- Carte 2 LAN : IP 192.168.1.1/24, GW 172.16.26.2, DNS 127.0.0.1, 192.168.1.2, FDN |        |
| **serveur** Microsoft Windows<br />(réseau WAN & LAN) | - Carte 1 WAN : IP 172.16.26.132/24, GW 172.16.26.2, DNS 172.16.26.2<br />- Carte 2 LAN : IP 192.168.1.2/24, GW 192.168.1.1, DNS 192.168.1.1 | serveur Active Directory simple<br />avec profils bureaux itinérants |
| **client** GNU/Linux Ubuntu<br />(réseau LAN) | - Carte 1 LAN : IP 192.168.0.50/24, GW 192.168.1.1, DNS 192.168.1.1 | client VPN |
| **client** Microsoft Windows<br />(réseau LAN) | - Carte 1 LAN : IP 192.168.0.51/24, GW 192.168.1.1, DNS 192.168.1.1 | client VPN |

##### 2) Administration à distance.

1. **DÉSACTIVER** le filtrage via le WAN : `pfctl -d`.
2. **AJOUTER** une règle dans le pare-feu comme suit:
```
Action : "Pass"
Interface : "WAN"
Source : "Single host or alias" et "172.16.26.129"
Destination : "WAN Address"
Destination port range : "HTTPS (443)"
Log : Activer la journalisation sur cette règle.
Description : "GUI WAN access"
```
3. **RÉACTIVER** le filtrage via le WAN : `pfctl -e`.
4. L'administration à distance est désormais possible via l'adresse WAN, par exemple depuis la machine hôte d'une machine virtuelle pfSense.
5. **VÉRIFIER** dans les interfaces que les réseaux privés ne sont pas bloqués, dans le cadre d'un test en réseau local.

##### 3) Ajout de paquets supplémentaires.

Depuis le menu _Système > Gestionnaire de paquets_, **INSTALLER** _openvpn-client-export_.

##### 4) DHCP & DNS.

<center><img src="../../../../assets/img/tuto/pfsense2.png" alt="pfsense2" /></center><br />

1. **ADAPTER** dans _État > Bails DHCP_ le mappage statique pour le serveur Active Directory et les baux DHCP (range, durée, etc ...).
1. **AJOUTER** dans _Services > Serveur DHCP > LAN_ l'adresse IP du serveur Active Directory dans les serveurs DNS du LAN (pour assurer la découverte du domaine via les clients locaux).

#### B) Configuration.

<center><img src="../../../../assets/img/tuto/pfsense3.png" alt="pfsense3" /></center><br />

##### 1) Création de l'Autorité de Certification (CA) et du Certificat Serveur.

Depuis le menu VPN > Gestionnaire de certificats :
1. **CRÉER** le certificat ACs (autorité).
1. **CRÉER** le certificat Serveur (interne).

##### 2) Création du tunnel VPN.

<center><img src="../../../../assets/img/tuto/pfsense4.png" alt="pfsense4" /></center><br />
<center><img src="../../../../assets/img/tuto/pfsense5.png" alt="pfsense5" /></center><br />
<center><img src="../../../../assets/img/tuto/pfsense6.png" alt="pfsense6" /></center><br />

##### 3) Création et activation d'utilisateurs locaux.



### Annexe
#### Sources tierces
- https://doc.ubuntu-fr.org/vpn
- https://doc.ubuntu-fr.org/openvpn
- https://neptunet.fr/pfsense-install/
- https://www.it-connect.fr/pfsense-activer-lacces-a-linterface-web-depuis-wan/
- https://superuser.com/questions/271740/how-to-set-up-vpn-connection-with-p12-and-ovpn-file
- https://askubuntu.com/questions/1046458/how-to-add-openvpn-connection-in-the-gui-using-ovpn-p12-and-key-files
- https://memo-linux.com/configurer-networkmanager-pour-se-connecter-a-un-serveur-openvpn/
- vidéo IT-Connect : https://www.youtube.com/watch?v=QDaRpaIH4GE
